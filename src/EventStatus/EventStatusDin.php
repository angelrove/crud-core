<?php
/**
 * @author José A. Romero Vegas <jangel.romero@gmail.com>
 *
 */

namespace angelrove\CrudCore\EventStatus;


class EventStatusDin
{
    public $listObjects = [];

    /*
     * Objects
     */

    public function createObject(string $objectId): void
    {
        if (isset($this->listObjects[$objectId])) {
            return;
        }

        $this->listObjects[$objectId] = new ObjectStatus();
        $this->listObjects[$objectId]->ROW_ID = '';

        // dump('New object:'. $objectId);
    }

    public function getObject(string $objectId): ObjectStatus
    {
        if (!isset($this->listObjects[$objectId])) {
            throw new \Exception("EventStatus error: the objectId '$objectId' not found!");
        }

        return $this->listObjects[$objectId];
    }

    public function getObjectOrCreate(string $objectId): ObjectStatus
    {
        if (!isset($this->listObjects[$objectId])) {
            $this->createObject($objectId);
        }

        return $this->getObject($objectId);
    }

    /*
     * Data
     */

    public function setData(string $objectId, string $name, $value): void
    {
        $this->listObjects[$objectId]->data[$name] = $value;
    }

    public function setDataDefault(string $objectId, string $name, $value = ''): void
    {
        if (!isset($this->listObjects[$objectId]->data[$name])) {
            $this->listObjects[$objectId]->data[$name] = $value;
        }
    }

    public function getData(string $objectId, string $name): ?string
    {
        return ($this->listObjects[$objectId]->data[$name])?? null;
    }
}
