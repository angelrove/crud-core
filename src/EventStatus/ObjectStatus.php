<?php
/**
 * @author Jos� A. Romero Vegas <jangel.romero@gmail.com>
 *
 */

namespace angelrove\CrudCore\EventStatus;


class ObjectStatus
{
    public $ROW_ID;
    public $data = [];
}
