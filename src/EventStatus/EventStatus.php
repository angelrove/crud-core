<?php
/**
 * @author José A. Romero Vegas <jangel.romero@gmail.com>
 */

namespace angelrove\CrudCore\EventStatus;

use angelrove\CrudCore\EventStatus\Session;

class EventStatus
{
    public static $DEBUG = false;

    public static $CONTROL;
    public static $EVENT;
    public static $EVENT_PREV;
    public static $ROW_ID;

    private static EventStatusDin $eventStatus;

    public static function init(): void
    {
        Session::start();

        // EventStatusDin object ---
        if (Session::get('EventStatus')) {
            self::$eventStatus = Session::get('EventStatus');
        } else {
            self::$eventStatus = Session::set('EventStatus', new EventStatusDin());
        }

        // Parse events ---
        self::parseEvent();
    }

    public static function destroy(): void
    {
        Session::destroy();
    }

    public static function debug(): void
    {
        self::$DEBUG = true;

        // dump(\Request::route()->getName());
        // dump(\Request::route()->parametersWithoutNulls());
        // dump(\Request::all());
        // echo "Objects"; dump(self::$eventStatus->listObjects);

        // dump(request()->path());
        // dump('is edit: '.request()->is('*/edit'));
    }

    /*
     * Objects
     */

    public static function createObject(string $objectId): void
    {
        self::$eventStatus->createObject($objectId);
    }

    /*
     * Events
     */
    private static function parseEvent(): void
    {
        if(!$_REQUEST && !\Request::route()) {
            return;
        }

        /*
         * Get current object: REQUEST['objectId'] or current Section
         */
        $objectId = (isset($_REQUEST['objectId']))?
            $_REQUEST['objectId'] :
            self::getCurrentObject();

        $object = self::$eventStatus->getObjectOrCreate($objectId);

        // Event ---
        if(isset($_REQUEST['event'])) {
            self::setEvent($objectId, $_REQUEST['event']);
        }

        // ROW_ID ---
        if(isset($_REQUEST['id'])) {
            self::$ROW_ID = $_REQUEST['id'];
            self::setRowId($objectId, self::$ROW_ID);
        }
        elseif ($params = \Request::route()->parametersWithoutNulls()) {
            $id = array_values($params)[0];
            if (!is_numeric($id)) {
                return;
            }

            self::$ROW_ID = $id;
            self::setRowId($objectId, self::$ROW_ID);
        }

        // Data (only GET) ---
        // $object = self::$eventStatus->getObject($objectId);
        foreach ($_GET as $name => $value) {
            if ($name == 'event' || $name == 'objectId' || $name == 'id') {
            } else {
                $object->data[$name] = $value;
            }
        }

        // DEBUG ------
        if (self::$DEBUG == true) {
            dump("Parse event >>");
            dump("Current object: $objectId");
            dump(self::$eventStatus->listObjects);
        }
    }

    /*
     * From Request route
     */
    private static function getCurrentObject(): ?string
    {
        list($objectId) = explode('.', \Request::route()->getName());
        if (!$objectId) {
            $objectId = \Request::segment(1);
        }

        return $objectId;
    }

    private static function getCurrentId(): string
    {
        list($section) = explode('.', \Request::route()->getName());
        $rowId = \Request::segment(2);

        return $rowId;
    }

    /*
     * Event
     */

    public static function setEvent(string $objectId, string $event): void
    {
        self::$CONTROL = $objectId;

        //---
        Session::set('EventStatus_event_prev', Session::get('EventStatus_event'));
        self::$EVENT_PREV = Session::get('EventStatus_event_prev');

        //---
        Session::set('EventStatus_event', $event);
        self::$EVENT = $event;
    }

    public static function getEvent(string $objectId): ?string
    {
        return (self::$CONTROL == $objectId)? self::$EVENT : null;
    }

    public static function delEvent()
    {
        self::$EVENT = null;
    }

    /*
     * Data
     */
    public static function setData(?string $objectId, string $name, $value): void
    {
        $objectId = ($objectId)? $objectId : self::getCurrentObject();

        self::$eventStatus->setData($objectId, $name, $value);
    }

    public static function setDataDefault(string $objectId, string $name, $value = ''): void
    {
        self::$eventStatus->setDataDefault($objectId, $name, $value);
    }

    public static function getData(?string $objectId, string $name): mixed
    {
        $objectId = ($objectId)? $objectId : self::getCurrentObject();
        return self::$eventStatus->getData($objectId, $name);
    }

    public static function getAllData(?string $objectId): array
    {
        $objectId = ($objectId)? $objectId : self::getCurrentObject();
        return self::$eventStatus->getObject($objectId)->data;
    }

    // Get object data or create new object
    public static function getAllDataOrDefaults(?string $objectId, array $defaults = []): array
    {
        $objectId = ($objectId)? $objectId : self::getCurrentObject();

        $object = self::$eventStatus->getObjectOrCreate($objectId);

        // Set default values ---
        foreach ($defaults as $name => $value) {
            self::$eventStatus->setDataDefault($objectId, $name, $value);
        }

        return self::getAllData($objectId);
    }

    public static function delData(string $objectId, string $name): void
    {
        self::setData($objectId, $name, '');
    }

    /*
     * RowId
     */
    public static function setId(string $id): void
    {
        $objectId = self::getCurrentObject();

        self::$eventStatus->listObjects[$objectId]->ROW_ID = $id;
    }

    public static function getId(): string
    {
        $objectId = self::getCurrentObject();
        return self::$eventStatus->listObjects[$objectId]->ROW_ID;
    }

    public static function getRowId(string $objectId): string
    {
        return self::$eventStatus->listObjects[$objectId]->ROW_ID;
    }

    public static function delRowId(string $objectId): void
    {
        self::$eventStatus->listObjects[$objectId]->ROW_ID = '';
    }

    public static function setRowId(string $objectId, int $id): void
    {
        self::$eventStatus->listObjects[$objectId]->ROW_ID = $id;
    }
}
