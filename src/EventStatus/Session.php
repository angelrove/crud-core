<?php
/**
 *
 * @author José A. Romero Vegas <jangel.romero@gmail.com>
 */

namespace angelrove\CrudCore\EventStatus;


class Session
{
    public static function start(): void
    {
        if (session_status() === PHP_SESSION_NONE) {
            session_save_path(config('session.files'));
            session_start();
            // dd($_SESSION['EventStatus']);
        }
    }

    public static function destroy(): void
    {
        session_unset();
        session_destroy();
    }

    // return reference
    public static function set(string $key, $obj): mixed
    {
        $_SESSION[$key] = $obj;
        return $_SESSION[$key]; // devuelve una referencia
    }

    // return reference
    public static function get(string $key): mixed
    {
        return ($_SESSION[$key])?? null;
    }

    public static function unset(string $key): void
    {
        if (isset($_SESSION[$key])) {
            unset($_SESSION[$key]);
        }
    }

    // public static function set(string $key, $obj)
    // {
    //     session([$key => $obj]);
    // }

    // public static function get(string $key)
    // {
    //     return session($key);
    // }

    // public static function unset(string $key)
    // {
    //     session()->forget($key);
    // }
}
