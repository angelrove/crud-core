<?php
/**
 * @author José A. Romero Vegas <jangel.romero@gmail.com>
 *
 */

namespace angelrove\CrudCore;

use angelrove\CrudCore\EventStatus\EventStatus;
use Illuminate\Support\Facades\View;

class CrudCore
{
    public const VENDOR_DIR = 'vendor/crudcore';

    public static function init(bool $debug=false): void
    {
        EventStatus::$DEBUG = $debug;

        EventStatus::init();
        // EventStatus::debug();

        // Backpack: views namespace
        View::addNamespace('CrudCore', CrudCore::views_path());
    }

    public static function assets_path(?string $route=''): string
    {
        return public_path(self::VENDOR_DIR.'/'.$route);
    }

    public static function views_path(?string $route=''): string
    {
        return base_path('resources/views/'.self::VENDOR_DIR.'/'.$route);
    }

    public static function components_path(?string $route=''): string
    {
        return base_path('resources/views/components/crudcore/'.$route);
    }
}
