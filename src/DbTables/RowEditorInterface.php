<?php
namespace angelrove\CrudCore\DbTables;

interface RowEditorInterface
{
    public function getBgColorAt($id, $idSelected, &$values);
    public function getClass($id, $idSelected, &$values);
}
