<?php
namespace angelrove\CrudCore\DbTables;

use angelrove\CrudCore\CrudUrl;
use angelrove\CrudCore\EventStatus\EventStatus;
use angelrove\CrudCore\Local;

trait RowsTrait
{
    //-------------------------------------------------------
    private function getHtmHead(): string
    {
        $orderSimbols = [
            'none' => '<i class="fas fa-sort"></i> &nbsp;',
            'down' => '<i class="fas fa-sort-amount-down-alt"></i> &nbsp;',
            'up'   => '<i class="fas fa-sort-amount-up"></i> &nbsp;',
        ];

        /** Títulos de los campos **/
        $order_column = EventStatus::getData($this->id_object, 'order_column');
        $order        = EventStatus::getData($this->id_object, 'order');

        $orderSimbol = ($order == 'DESC')? $orderSimbols['up'] : $orderSimbols['down'];

        $htmTitles = '';
        foreach ($this->dbFields as $dbField) {
            if (!$dbField) {
                continue;
            }

            // Oder ---
            if ($dbField->order) {
                $simbol = ($order_column == $dbField->order) ? $orderSimbol : $orderSimbols['none'];
                $link = str_replace('#column#', $dbField->order, $this->event_order);

                $dbField->title = '<a class="btFieldOrder" href="'.$link.'">'.$simbol.$dbField->title.'</a>';
            }

            // width ---
            $width = '';
            if ($dbField->width) {
                $width = " style='min-width: $dbField->width;'";
            }

            // OnClick ---
            if ($dbField->onClick) {
                $link = CrudUrl::getRouteParams(
                    $this->event_fOnClick,
                    $this->id_object,
                    '',
                    ['onClick_field'=>$dbField->onClick, 'page'=>'0']
                );

                $dbField->title = '<a class="btFieldOrder" href="' . $link . '">' . $dbField->title . '</a>';
            }

            // Out
            $htmTitles .= '<th scope="col" id="fieldTitle_'.$dbField->name.'"'.$width.'>'.$dbField->title.'</th>';
        }

        /** Botón 'Nuevo...' **/
        $strBt_new = '';
        if ($this->bt_new) {
            $strBt_new = '<button type="button" class="on_new btn btn-primary btn-sm">'.
                            '<i class="fas fa-plus" aria-hidden="true"></i> New...'.
                         '</button>';
        }

        return $htmTitles.'<th scope="col" class="optionsBar" id="List_cabButtons_'.$this->id_object.'">'.$strBt_new.'</th>';
    }
    //-------------------------------------------------------
    private function getHtmRows(iterable $rows): string
    {
        if (!$rows) {
            return '';
        }

        // $row_id ---
        $row_id = EventStatus::getRowId($this->id_object);

        /** Rows **/
        $htmList = '';
        $count   = 0;
        foreach ($rows as $key => $row) {
            // Row id ---
            $id = ($row->id)?? $key;

            /** RowEditor **/
            $row_bgColor = '';
            $row_class   = '';
            if (isset($this->rowEditor)) {
                $row_bgColor = $this->rowEditor->getBgColorAt($id, $row_id, $row);
                $row_class   = $this->rowEditor->getClass($id, $row_id, $row);
            }

            /** Data **/
            $strCols = '';
            foreach ($this->dbFields as $dbField) {
                if (!$dbField) {
                    continue;
                }

                $strField = $this->getHtmCell($id, $row, $dbField);
                if ($strField == "EXIT_ROW") {
                    $strCols = '';
                    break;
                }
                $strCols .= $strField;
            }
            if ($strCols == '') {
                continue;
            }

            /** Option buttons **/
            $strButtons = $this->getHtmButtons($id, $row, ++$count);

            /** Row color **/
            $styleColor = ($row_bgColor) ? " style=\"background:$row_bgColor\"" : '';

            /** Sucess **/
            $styleSucess = ($row_id == $id)? ' class="table-primary"' : '';
            $styleSucess = ($row_class)? " class=\"$row_class\"" : $styleSucess;

            /** Tupla **/
            $htmList .= "<tr id='$id'".$styleSucess.$styleColor.">$strCols $strButtons</tr>";
        }

        return $htmList;
    }
    //-------------------------------------------------------
    // Buttons
    //-------------------------------------------------------
    private function getHtmButtons(int $id, object $row, int $numRow): string
    {
        $htmButtons = array();

        /** Buttons **/
        $btStyles = 'btn btn-link btn-sm';

        if ($this->bt_update) {
            $label                   = ''; // $label = <span>Update</span>
            $htmButtons['bt_update'] = '<button type="button" class="on_update '.$btStyles.'">'.
                                          '<i class="far fa-edit"></i>' . $label .
                                       '</button>';
            if ($this->optionsEditor && $this->optionsEditor->showBtUpdate($id, $row) === false) {
                $htmButtons['bt_update'] = '';
            }
        }

        if ($this->bt_delete) {
            $label                   = ''; // $label = <span>Delete</span>
            $htmButtons['bt_delete'] = '<button type="button" class="on_delete '.$btStyles.'">'.
                                          '<i class="far fa-trash-alt"></i>'.$label .
                                       '</button>';
            if ($this->optionsEditor && $this->optionsEditor->showBtDelete($id, $row) === false) {
                $htmButtons['bt_delete'] = '';
            }
        }

        if ($this->bt_detalle) {
            $label                    = ''; // $label = <span>Detalle</span>
            $htmButtons['bt_detalle'] = '<button type="button" class="on_detail '.$btStyles.'">'.
                                          '<i class="fas fa-caret-right"></i>' . $label .
                                        '</button>';
        }

        // Opcional ----
        foreach ($this->list_Op as $key => $bt_opc) {
            // optionsEditor ----
            $ret_optionsEditor = '';

            if ($this->optionsEditor) {
                $ret_optionsEditor = $this->optionsEditor->showBtOp($key, $id, $row);

                if ($ret_optionsEditor === false) {
                    // ocultar
                    continue;
                } elseif ($ret_optionsEditor === true) { // mostrar
                    // show
                } elseif (is_array($ret_optionsEditor)) {
                    // parámetros
                    if (isset($ret_optionsEditor['label'])) {
                        $bt_opc['label'] = $ret_optionsEditor['label'];
                    }
                    if (isset($ret_optionsEditor['disabled'])) {
                        $bt_opc['disabled'] = $ret_optionsEditor['disabled'];
                    }
                    $bt_opc['href']   = $ret_optionsEditor['href'];
                    $bt_opc['target'] = $ret_optionsEditor['target'];
                }
            }

            //--------
            $bt_href = CrudUrl::getRoute($bt_opc['event'], $this->id_object, $id);

            if ($bt_opc['href']) {
                $bt_href = $bt_opc['href'];
            }

            $bt_target = '';
            if ($bt_opc['target']) {
                $bt_target = $bt_opc['target'];
            }
            if ($bt_opc['disabled'] === true) {
                $htmButtons['op_' . $key] = '<span class="disabled_' . $key . '">' .
                                                $bt_opc['label'] .
                                            '</span>';
            } else {
                $htmButtons['op_' . $key] = "<a class='$key btn btn-primary btn-sm' ".
                                               "role='button' ".
                                               "href='$bt_href' ".
                                               "target='$bt_target' ".
                                               "title='$bt_opc[title]'>$bt_opc[label]</a>";
            }
        }
        // END Opcional ---

        /** Out **/
        return '<td class="optionsBar">' . implode('', $htmButtons) . '</td>';
    }
    //--------------------------------------------------------------

}