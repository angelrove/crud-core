<?php

/**
 * @author José A. Romero Vegas <jangel.romero@gmail.com>
 */

namespace angelrove\CrudCore\DbTables;

use angelrove\CrudCore\EventStatus\EventStatus;
use angelrove\CrudCore\CrudCore;
use angelrove\CrudCore\Local;
use angelrove\CrudCore\JsLoad;
use angelrove\CrudCore\CssLoad;
use angelrove\CrudCore\CrudUrl;

class DbTables
{
    use DataTrait;
    use RowsTrait;
    use CellTrait;
    use EventsTrait;
    use SearchTrait;

    public $id_object;

    //-------
    private $sqlQuery;
    private $dbFields = [];
    private $title;
    private $listRows;

    private $height;
    private $msgConfirmDel = '';
    private $showScroll;
    private $noId = false;
    private $rowsAdd = false;

    // Pagination
    private $htmPaginacion;
    private $paging_show  = true;
    private $paging_numRows = 100;

    // Buttons
    private $onClickRow = '';
    private $list_Op = array();

    private $bt_new            = false;
    private $bt_update         = false;
    private $bt_detalle        = false;
    private $bt_delete         = false;
    private $bt_delete_confirm = true;

    // Editors
    private $rowEditor;
    private $cellEditor;
    private $optionsEditor;

    //-------------------------------------------------------
    // PUBLIC
    //-------------------------------------------------------
    /**
     * @param $data_mixed: [
     *          array data,
     *          \Illuminate\Database\Eloquent\Builder object
     *          \Illuminate\Pagination\LengthAwarePaginator object
     *        ]
     */
    public function __construct(string $objectId, mixed $data = null, array $columns = [])
    {
        Local::_init();

        if (!$objectId) {
            throw new \Exception("DbTables: the parameter 'objectId' cannot be empty.
                It is necessary to save the state of it in the session");
        }

        $this->id_object = $objectId;
        $this->sqlQuery = $data;
        $this->dbFields = $columns;

        // $this->loadLangFiles();


        $this->eventsInit();

        // css / js ------
        JsLoad::add(CrudCore::VENDOR_DIR . '/DbTables/lib.js');
        CssLoad::add(CrudCore::VENDOR_DIR . '/DbTables/style.css');
    }
    //------------------------------------------------------
    public function reload(): void
    {
        EventStatus::delData($this->id_object, 'page');
    }
    //-------------------------------------------------------
    // Conf
    //--------------------------------------------------------------
    public function setData(string $data): void
    {
        $this->sqlQuery = $data;
    }
    //--------------------------------------------------------------
    public function setRowId(string $rowId = null): void
    {
        if ($rowId) {
            EventStatus::setId($this->id_object, $rowId);
        }
    }
    //--------------------------------------------------------------
    public function column(string $name, string $title, string $type = ''): DtColumn
    {
        $column = new DtColumn($name, $title, '', '', $type);
        $this->dbFields[] = $column;

        return $column;
    }
    //--------------------------------------------------------------
    public function setDefaultOrder(string $column, string $order = 'ASC'): void
    {
        $order_column = EventStatus::getData($this->id_object, 'order_column');
        if ($order_column) {
            return;
        }

        //----
        EventStatus::setData($this->id_object, 'order_column', $column);
        EventStatus::setData($this->id_object, 'order_column_prev', $column);
        EventStatus::setData($this->id_object, 'order', $order);
    }
    //------------------------------------------------------
    private static function loadLangFiles(): void
    {
        include_once __DIR__ . '/local_t/en.inc';
    }
    //--------------------------------------------------------------
    // Setter
    //-------------------------------------------------------
    public function title(?string $title): void
    {
        $this->title = $title;
    }
    //-------------------------------------------------------
    public function setColumns(array $dbFields): void
    {
        $this->dbFields = $dbFields;
    }
    //-------------------------------------------------------
    public function setReadOnly(bool $isReadonly): void
    {
        if ($isReadonly) {
            $this->bt_new    = false;
            $this->bt_delete = false;
        }
    }
    //-------------------------------------------------------
    public function setNoId(): void
    {
        $this->noId = true;
    }
    //-------------------------------------------------------
    public function setScroll(int $height): void
    {
        $this->showScroll = true;
        $this->height     = $height;
    }
    //-------------------------------------------------------
    // Editors
    //-------------------------------------------------------
    public function setRowEditor(RowEditorInterface $rowEditor): void
    {
        $this->rowEditor = $rowEditor;
    }
    //-------------------------------------------------------
    public function setColumnEditor(CellEditorInterface $cellEditor): void
    {
        $this->cellEditor = $cellEditor;
    }
    //-------------------------------------------------------
    public function setOptionsEditor(CellOptionsEditorInterface $optionsEditor): void
    {
        $this->optionsEditor = $optionsEditor;
    }
    //-------------------------------------------------------
    // Events: CRUD
    //-------------------------------------------------------
    public function showNew(bool $showButton = true): void
    {
        $this->bt_new = $showButton;
    }
    //-------------------------------------------------------
    public function showUpdate(bool $showButton = false): void
    {
        if ($showButton === true) {
            $this->bt_update = true;
            if (!$this->onClickRow) {
                // si no se ha asignado previamente
                $this->onClickRow = $this->event_update;
            }
        } else {
            $this->onClickRow = $this->event_update;
        }
    }
    //-------------------------------------------------------
    public function showDelete(bool $isConfirm = true): void
    {
        $this->bt_delete         = true;
        $this->bt_delete_confirm = $isConfirm;

        if ($this->bt_delete_confirm) {
            $this->msgConfirmDel = '¿Estás seguro?';
        }
    }
    //-------------------------------------------------------
    public function showDetail(bool $showButton = true): void
    {
        if ($showButton === true) {
            $this->bt_detalle = $showButton;
        }
        $this->onClickRow = $this->event_detail;
    }
    //-------------------------------------------------------
    public function getOptionButton(string $event, string $label): string
    {
        $url = CrudUrl::getRoute($event);

        $htmButton =
            "<a class='$event btn btn-primary btn-sm' " .
            "role='button' " .
            "href='$url' " .
            "target='' " .
            "title=''>$label</a>";

        return $htmButton;
    }
    //-------------------------------------------------------
    // User options
    //-------------------------------------------------------
    public function setBtOpc(string $event, string $label = '', string $onClick = null, string $title = ''): void
    {
        $this->list_Op[$event] = array(
            'event'    => $event,
            'oper'     => $event,
            'label'    => $label,
            'onClick'  => $onClick,
            'title'    => $title,
            'href'     => '',
            'target'   => '',
            'disabled' => '',
        );
    }
    //-------------------------------------------------------
    public function onClickRow(string $event): void
    {
        $this->onClickRow = $event;
    }
    //-------------------------------------------------------
    // Pagination
    //-------------------------------------------------------
    public function showPagination(bool $show, int $numRows = 100): void
    {
        $this->paging_show  = $show;
        $this->paging_numRows = $numRows;
    }
    //-------------------------------------------------------
    public function setNumRowsPage(int $numRows): void
    {
        $this->paging_numRows = $numRows;
    }
    //-------------------------------------------------------
    public function addRows(array $rows): void
    {
        $this->rowsAdd = $rows;
    }
    //-------------------------------------------------------
    // OUT
    //-------------------------------------------------------
    public function get(): string
    {
        $controlID = $this->id_object;

        /** >> htmPaginacion, listRows **/
        list($this->htmPaginacion, $this->listRows) = $this->getData();

        /** Add row **/
        if ($this->rowsAdd) {
            $this->listRows = $this->rowsAdd + $this->listRows;
        }

        /** >> htmPaginacion **/
        if ($this->paging_show === 'false') {
            $this->htmPaginacion = '';
        }
        $htmPaginacion = &$this->htmPaginacion;

        /** >> $htmListDatos **/
        $htmListDatos = $this->getHtmRows($this->listRows);

        /** >> $htmColumnas **/
        $htmColumnas = $this->getHtmHead();

        /** OUT **/
        ob_start(); /* ¡¡Para retornar el contenido del include!! */
        include __DIR__ . '/views/tmpl_list.php';
        return ob_get_clean();
    }
    //-------------------------------------------------------
}
