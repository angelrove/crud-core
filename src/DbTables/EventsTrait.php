<?php
namespace angelrove\CrudCore\DbTables;

use angelrove\CrudCore\EventStatus\EventStatus;
use angelrove\CrudCore\CrudUrl;

trait EventsTrait
{
    // Events ---
    private $event_cancel;
    private $event_new;
    private $event_update;
    private $event_delete;
    private $event_detail;

    private $action_search;
    private $param_action;

    private $event_order;
    private $event_page;
    private $event_fOnClick;


    private function eventsInit(): void
    {
        $this->eventsRoutes();

        // Init data defaults ---
        EventStatus::createObject($this->id_object);
        EventStatus::setDataDefault($this->id_object, 'order_column');
        EventStatus::setDataDefault($this->id_object, 'order_column_prev');
        EventStatus::setDataDefault($this->id_object, 'order');
        EventStatus::setDataDefault($this->id_object, 'page');
        EventStatus::setDataDefault($this->id_object, 'search');
        EventStatus::setDataDefault($this->id_object, 'detail');

        // Events ---
        $this->parseEvent();
    }

    private function eventsRoutes(): void
    {
        $this->event_cancel = CrudUrl::getRoute();
        $this->event_new    = CrudUrl::getRoute('create');
        $this->event_update = CrudUrl::getRoute('edit', true);
        $this->event_delete = CrudUrl::getRoute('destroy', true);

        $this->event_page   = CrudUrl::getRouteParams('page',   $this->id_object, false, ['page'=>'#page#']);
        $this->event_order  = CrudUrl::getRouteParams('order',  $this->id_object, false, ['order_column'=>'#column#']);
        $this->event_detail = CrudUrl::getRouteParams('detail', $this->id_object, false, ['id'=>'#id#']);

        $this->action_search = CrudUrl::getRoute();
        $this->param_action = CrudUrl::getRoute();
        // $this->event_fOnClick = 'fieldOnClick';
    }
    //-------------------------------------------------------
    /*
     * Event actions: pagination, column order,...
     */
    public function parseEvent(): void
    {
        // Eventos de este DbList ---
        $event = EventStatus::getEvent($this->id_object);
        $data  = EventStatus::getAllData($this->id_object);

        switch ($event) {
            case 'order':
                $this->onColumnOrder($data);
                break;

            case 'search':
                break;

            case 'detail':
                // reiniciar la paginación
                EventStatus::delData($this->id_object, 'page');
                break;
        }

        // Eventos de cualquier DbList ---
        switch (EventStatus::$EVENT) {
            case 'detail':
                // reiniciar la paginación
                EventStatus::delData($this->id_object, 'page');
                break;
        }
    }

    private function onColumnOrder($data): void
    {
        $column     = $data['order_column'];
        $columnPrev = $data['order_column_prev'];
        $order      = $data['order'];

        // revert order
        if ($columnPrev == $column) {
            $order = ($order == 'DESC') ? 'ASC' : 'DESC';
            EventStatus::setData($this->id_object, 'order', $order);
        }
        EventStatus::setData($this->id_object, 'order_column_prev', $column);

        // reiniciar la paginación
        EventStatus::delData($this->id_object, 'page');
    }
}