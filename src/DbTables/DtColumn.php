<?php
/**
 * @author José A. Romero Vegas <jangel.romero@gmail.com>
 *
 */

namespace angelrove\CrudCore\DbTables;

class DtColumn
{
    public $name;
    public $title;
    public $width;
    public $max_width;
    public $align = '';
    public $relation = '';
    public $type = '';
    public $typeOptions;
    public $order;
    public $class;
    public $onClick;
    public $preventDefault;

    //-------------------------------------------------------
    public function __construct(string $name, string $title)
    {
        $this->name  = $name;
        $this->title = $title;
    }
    //-------------------------------------------------------
    public function relation(string $relation): self
    {

        $this->relation = $relation;
        return $this;
    }
    //-------------------------------------------------------
    public function type(string $type, $typeOptions = null): self
    {
        $this->type = $type;
        $this->typeOptions = $typeOptions;
        return $this;
    }
    //-------------------------------------------------------
    public function sortable(string $field = ''): self
    {
        $this->order = (!$field)? $this->name : $field;
        return $this;
    }
    //-------------------------------------------------------
    public function width($width): self
    {
        $this->width = $width.'px';
        return $this;
    }
    //-------------------------------------------------------
    public function align(string $align): self
    {
        $this->align = $align;
        return $this;
    }
    //-------------------------------------------------------
    public function setClass(): self
    {
        $this->class = $this->name;
        return $this;
    }
    //-------------------------------------------------------
    public function onClick(): self
    {
        $this->onClick = $this->name;
        return $this;
    }
    //-------------------------------------------------------
    public function preventDefault(): self
    {
        $this->preventDefault = true;
        return $this;
    }
    //-------------------------------------------------------
}
