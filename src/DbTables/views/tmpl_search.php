<?php
use angelrove\CrudCore\CrudUrl;

$action = CrudUrl::getRouteParams('detail', $this->id_object);
?>

<form class="FormSearch form-inline" method="get" action="<?=$action?>" role="search">
    <?=$this->inputSearch()?>
</form>

<script>
document.querySelector('.FormSearch input').onchange = function()
{
    document.querySelector('.FormSearch').submit();
}
</script>
