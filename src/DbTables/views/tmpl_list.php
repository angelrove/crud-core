<?php
// Scroll ----
$class_scroll = '';
$styleScroll  = '';
if ($this->showScroll == true) {
    $class_scroll = 'setScroll';
    $styleScroll  = 'height:' . $this->height . 'px;';
}
?>

<!-- Wlist -->
<script>
var List_msgConfirmDel = '<?=$this->msgConfirmDel?>';
</script>

<?php
if ($this->search) {
   include('tmpl_search.php');
}
?>

<div class="List" id="List_<?=$this->id_object?>">
    <div class="List_paginacion">
        <?=$htmPaginacion['resume']?>
    </div>

    <div class="table-responsive <?=$class_scroll?>" style="<?=$styleScroll?>">
        <table class="List_tuplas
                      table
                      table-striped
                      table-bordered
                      table-hover"
               param_action="<?=$this->param_action?>"

               param_event-onRow ="<?=$this->onClickRow?>"
               param_event-new   ="<?=$this->event_new?>"
               param_event-update="<?=$this->event_update?>"
               param_event-delete="<?=$this->event_delete?>"
               param_event-detail="<?=$this->event_detail?>">
           <thead>
                <?php if ($this->title) { ?>
                   <tr><th class="text-center" colspan="<?=count($this->dbFields) + 1?>"><?=$this->title?></th></tr>
                <?php } ?>
               <tr><?=$htmColumnas?></tr>
           </thead>
           <tbody><?=$htmListDatos?></tbody>
        </table>
    </div>

    <!-- Pagination -->
    <?php include __DIR__.'/tmpl_pagination.php'; ?>
</div>
<!-- /Wlist -->
