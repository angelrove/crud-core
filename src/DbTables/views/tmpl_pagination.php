<?php
if (!$this->paging_show) {
    return;
}
?>

<div class="List_paginacion clearfix">
    <div style="float:left">
        <?=$htmPaginacion['resume']?>
    </div>

    <div style="float:right">
        <ul class="pagination">
            <?=$htmPaginacion['pages']?>
        </ul>
    </div>
</div>
