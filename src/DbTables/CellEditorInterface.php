<?php
namespace angelrove\CrudCore\DbTables;

interface CellEditorInterface
{
    public function getValueAt(int $id, string $columnName, string $value, array &$values);
    public function getBgColorAt(int $id, string $columnName, string $value, array $values);
}
