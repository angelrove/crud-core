<?php
namespace angelrove\CrudCore\DbTables;


interface CellOptionsEditorInterface
{
  public function showBtDelete(int $id, array $values);
  public function showBtUpdate(int $id, array $values);
  /*
   * return:
   *     true/false
   *     array('label'=>'[lable]', 'disabled'=>[true/false], 'href'=>'[xxx]');
   */
  public function showBtOp(string $key, int $id, array $values);
}
