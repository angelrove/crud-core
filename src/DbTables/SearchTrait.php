<?php
namespace angelrove\CrudCore\DbTables;

use angelrove\CrudCore\EventStatus\EventStatus;
use angelrove\CrudCore\CrudUrl;
use angelrove\CrudCore\Local;

trait SearchTrait
{
    private $search;
    private $search_placeholder;

    //--------------------------------------------------------------
    public function search(string $placeholder = ''): void
    {
        $this->search = true;
        $this->search_placeholder = $placeholder;
    }
    //--------------------------------------------------------------
    // Always submit form on change selects
    public function searcher(): string
    {
        $action = CrudUrl::getSeccRoute();
        $params = CrudUrl::getParams(method: 'POST', event: 'search', objectId: $this->id_object);

        return <<<EOD
<form class="FormSearch form-inline well well-sm"
      role="search"
      name="search_form"
      method="get"
      action="$action">
      $params

EOD;
    }
    //--------------------------------------------------------------
    public function searcher_END(): string
    {
        // Always submit form on change selects (lib.js)
        return <<<EOD
</form>
EOD;
    }
    //-------------------------------------------------------
    public function inputSearch(?string $placeholder=''): string
    {
        if(!$placeholder) {
            $placeholder = $this->search_placeholder;
        }

        if(!$placeholder) {
            $placeholder = Local::$t['Search'];
        }

        $value = EventStatus::getData($this->id_object, 'search');

        //---
        $deltext = ($value)? '<i class="fas fa-times" style="color:#999"></i>' : '';

        //---
        return <<<EOD
<input type="text" name="search" placeholder="$placeholder" value="$value"
       class="form-control input-sm"><a href="#" class="clear_search">$deltext</a>
&nbsp;
EOD;
    }
    //-------------------------------------------------------
    public function searcher_button(): void
    {
        echo '&nbsp;<button type="submit" class="btn btn-primary btn-sm">'.Local::$t['Search'].'</button>';
    }
    //--------------------------------------------------------------
}