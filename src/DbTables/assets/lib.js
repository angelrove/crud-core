
/**
 * DbTables
 * José Angel Romero <jangel.romero@gmail.com>
 * 2009
 */

//-- Scroll to previous position ----------------------
// set scroll position in session
$(window).scroll(function () {
    sessionStorage.scrollPos = $(window).scrollTop();
});

// get scroll position
var init = function () {
    $(window).scrollTop(sessionStorage.scrollPos || 0)
};
window.onload = init;
//-----------------------------------------------------


//-----------------------------------------------------
/** Events **/
//-----------------------------------------------------

// Form search ----------------------------------------
document.querySelector('.FormSearch input, .FormSearch select').onchange = function()
{
    document.querySelector('.FormSearch').submit();
}
document.querySelector('.FormSearch .clear_search').onclick = function()
{
    document.querySelector(".FormSearch input[name=search]").value = '';
    document.querySelector('.FormSearch').submit();
}

$(document).ready(function()
{
  // onRow ----------------------------------------------
  $('.List_tuplas td:not(.optionsBar, .onClickUser, .preventDefault)').click(function(event) {
    var row_id = $(this).parents("tr").attr('id');
    List_onEvent($(this), row_id, 'onRow', '', '');
  });
  // new ------------------------------------------------
  $('.List_tuplas .on_new').click(function(event) {
    List_onEvent($(this), '', 'new', '', '');
  });
  // update ---------------------------------------------
  $('.List_tuplas .on_update').click(function(event) {
    event.preventDefault();
    var row_id = $(this).parents("tr").attr('id');
    List_onEvent($(this), row_id, 'update', '', '');
  });
  // delete ---------------------------------------------
  $('.List_tuplas .on_delete').click(function(event) {
    event.preventDefault();
    var row_id = $(this).parents("tr").attr('id');
    List_onEvent($(this), row_id, 'delete', '', List_msgConfirmDel);
  });
  // detalle --------------------------------------------
  $('.List_tuplas .on_detail').click(function(event) {
    event.preventDefault();
    var row_id = $(this).parents("tr").attr('id');
    List_onEvent($(this), row_id, 'detalle', '', '');
  });
  //-----------------------------------------------------
});

//-----------------------------------------------------
// Functions
//-----------------------------------------------------
function List_onEvent(object, row_id, bt, oper, txConfirm)
{
    var List = object.parents(".List_tuplas");

    // var action = List.attr('param_action');
    var href_event  = List.attr('param_event-'+bt);
    if(!href_event) {
        return false;
    }

    if (row_id) {
        href_event = href_event.replace('#id#', row_id);
    }
    // alert(bt+': '+href_event);return;

    // Confirm
    if(txConfirm == '') {
        location.href = href_event;
    } else if(confirm(txConfirm)) {
        location.href = href_event;
    }
}
//-------------------------------------------------------
/*
// Delete en segundo plano
function ListOnDelRowBack(parentId, numRow, hrefBt, delete_confirm) {
  if(delete_confirm == false) {
     List_delRow(parentId, numRow, hrefBt);
  }
  else if(confirm('Va a eliminar el registro. ¿Está seguro...?')) {
     List_delRow(parentId, numRow, hrefBt);
  }

  return false;
}
*/
//-------------------------------------------------------
// PRIVATE
//-------------------------------------------------------
/*
function List_delRow(parentId, numRow, url) {
  var result = util_httpRequest(url, false);
  if(result == 'OK') {
     List_delNode(parentId, numRow);
  }
  else {
     alert("ERROR: \n"+result);
  }
}
//-------------------------------------------------------
function List_delNode(parentId, numRow) {
  var elementContenedor = document.getElementById(parentId).getElementsByTagName('tbody')[0];
  var elementDel = elementContenedor.getElementsByTagName('TR')[numRow-1]

  elementContenedor.removeChild(elementDel);
}
//-------------------------------------------------------
*/