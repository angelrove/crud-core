//-----------------------------------------------------
/** shortcuts **/
//-----------------------------------------------------
$(document).keydown(function(e)
{
    var shortcuts_row = $(".List_tuplas tbody tr.info").index();

    //----------------
    // Ctrl+Up
    if(e.keyCode == 38 && e.ctrlKey)
    {
      if(shortcuts_row <= 0) {
        return;
      }
      shortcuts_row--;
      $(".List_tuplas tbody tr").eq(shortcuts_row).addClass("info");
      $(".List_tuplas tbody tr").eq(shortcuts_row+1).removeClass("info");
    }
    //----------------
    // Ctrl+Down
    else if(e.keyCode == 40 && e.ctrlKey)
    {
      if(!$(".List_tuplas tbody tr").eq(shortcuts_row+1).attr("id")) {
        return;
      }
      shortcuts_row++;
      $(".List_tuplas tbody tr").eq(shortcuts_row).addClass("info");
      $(".List_tuplas tbody tr").eq(shortcuts_row-1).removeClass("info");
    }
    //----------------
    // Ctrl+Insert (new, onRow, delete)
    else if(e.keyCode == 45 && e.ctrlKey)
    {
      List_onEvent($(".List_tuplas tr"), '', 'new', '', '');
    }
    //----------------
    // Ctrl+Enter
    else if(e.keyCode == 13 && e.ctrlKey)
    {
      var row_id = $(".List_tuplas tr.info").attr('id');
      List_onEvent($(".List_tuplas tr.info"), row_id, 'onRow', '', '');
    }
    //----------------
    // Ctrl+Supr
    else if(e.keyCode == 46 && e.ctrlKey)
    {
      var row_id = $(".List_tuplas tr.info").attr('id');
      List_onEvent($(".List_tuplas tr.info"), row_id, 'delete', '', List_msgConfirmDel);
    }
    //-----------------------------------
});
