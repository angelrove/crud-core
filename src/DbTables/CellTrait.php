<?php
namespace angelrove\CrudCore\DbTables;
use Illuminate\Database\Eloquent\Model;


trait CellTrait
{
    //-------------------------------------------------------
    private function getHtmCell(int $id, Model $row, DtColumn $dbField): string
    {
        $styles = [];

        // Value ---
        if ($dbField->relation) {
            $f_value = @$row->{$dbField->relation}->{$dbField->name};
        } else {
            $f_value = @$row->{$dbField->name};
        }

        //---
        $f_valueCampo = ($f_value->name)?? $f_value;

        /** Align **/
        if ($dbField->type == 'boolean') {
            $dbField->align = 'center';
        }

        if ($dbField->align) {
            $styles[] = 'text-align:' . $dbField->align;
        }

        /** CellEditor **/
        $bgColor = $this->cellEditor($id, $dbField->name, $f_value, $row);
        if ($bgColor) {
            $styles[] = $bgColor;
        }

        /** Parse data type **/
        $value_formatted = $this->formatValue($dbField->type, $f_valueCampo, $dbField->typeOptions);

        /** prevent default **/
        $class_prevDef = '';
        if ($dbField->preventDefault || $dbField->type == 'file_download') {
            $class_prevDef = ' preventDefault ';
        }

        /** OUT **/
        $strStyles = ($styles)? ' style="' . implode(';', $styles) . '"' : '';

        return '<td class="'.$class_prevDef.' '.$dbField->class.'"'.$strStyles.'>'.$value_formatted.'</td>';
    }
    //-------------------------------------------------------
    // Private
    //-------------------------------------------------------
    private function formatValue(string $type, ?string $value, $typeOptions = null): ?string
    {
        //--------------
        if ($type == 'boolean') {
            $value = ($value)? '<i class="fas fa-check fa-lg"></i>' : '';
            return $value;
        }

        //--------------
        if (!$value) {
            return $value;
        }

        //--------------
        switch ($type) {
            case 'datetime':
                // $timezone = ($typeOptions)? $typeOptions : \Login::$timezone;
                $timezone = ($typeOptions)? $typeOptions : null;
                $value = \Carbon\Carbon::parse($value)->timezone($timezone)->format('d/m/y H:i');
                break;

            case 'date':
                // $timezone = ($typeOptions)? $typeOptions : \Login::$timezone;
                $timezone = ($typeOptions)? $typeOptions : null;
                $value = \Carbon\Carbon::parse($value)->setTimezone($timezone)->format('d/m/Y');
                break;

            case 'file':
                $value = substr($value, 0, strpos($value, "#"));
                break;

            case 'enum':
                $value = $typeOptions[$value];
                break;

            case 'file_download':
                $fileData = \angelrove\utils\FileUploaded::getInfo2($value);
                $value = '<a target="_blank" download href="'.$fileData['url'].'"><i class="fas fa-download fa-lg"></i></a>';
                break;

            case 'file_image':
                $fileData = \angelrove\utils\FileUploaded::getInfo2($value);
                $value = '<img src="'.$fileData['ruta_th'].'">';
                break;

            case 'date':
                // $timezone = ($typeOptions)? $typeOptions : \Login::$timezone;
                $timezone = ($typeOptions)? $typeOptions : null;
                $value = \Carbon\Carbon::parse($value)->setTimezone($timezone)->format('d/m/Y');
                break;

            case 'money':
                $value = number_format($value, 2);
                break;

            case 'money_short':
                $value = $this->number_format_short($value);
                break;

            case 'url':
                $lavel = '';
                if ($typeOptions == 'hide') {
                    $lavel = 'View...';
                } else {
                    $lavel = str_replace(['http://', 'https://', 'www.'], '', $value);
                }

                $value = '<a target="_blank" href="'.$value.'">'.$lavel.'</a>';
                break;

            default:
                $value = nl2br($value);
        }

        return $value;
    }
    //-------------------------------------------------------
    private static function number_format_short(float $n, int $precision = 1): string
    {
        if ($n < 900) {
            // 0 - 900
            $n_format = number_format($n, $precision);
            $suffix = '';
        } else if ($n < 900000) {
            // 0.9k-850k
            $n_format = number_format($n / 1000, $precision);
            $suffix = 'K';
        } else if ($n < 900000000) {
            // 0.9m-850m
            $n_format = number_format($n / 1000000, $precision);
            $suffix = 'M';
        } else if ($n < 900000000000) {
            // 0.9b-850b
            $n_format = number_format($n / 1000000000, $precision);
            $suffix = 'B';
        } else {
            // 0.9t+
            $n_format = number_format($n / 1000000000000, $precision);
            $suffix = 'T';
        }

        // Remove unecessary zeroes after decimal. "1.0" -> "1"; "1.00" -> "1"
        // Intentionally does not affect partials, eg "1.50" -> "1.50"
        if ( $precision > 0 ) {
            $dotzero = '.' . str_repeat( '0', $precision );
            $n_format = str_replace( $dotzero, '', $n_format );
        }

        return $n_format . $suffix;
    }
    //-------------------------------------------------------
    private function cellEditor(int $id, string $name, $value, &$columns): ?string
    {
        if (!isset($this->cellEditor)) {
            return null;
        }

        // Value ---
        $newValue = $this->cellEditor->getValueAt($id, $name, $value, $columns);

        // Si no existe la columna '$name' le aplica el valor (si existe ser�
        // que la ha creado otra llamada a 'cellEditor')?
        if (!isset($columns->{$name})) {
            $columns->{$name} = $newValue;
        }

        // Bg color ---
        $bgColor = $this->cellEditor->getBgColorAt($id, $name, $value, $columns);
        if ($bgColor) {
            $bgColor = 'background:' . $bgColor . ';';
        }

        return $bgColor;
    }
    //-------------------------------------------------------
}