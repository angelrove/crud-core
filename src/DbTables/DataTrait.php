<?php
namespace angelrove\CrudCore\DbTables;

use angelrove\CrudCore\EventStatus\EventStatus;
use angelrove\CrudCore\CrudUrl;
use angelrove\CrudCore\Local;

trait DataTrait
{
    //--------------------------------------------------------------
    private function getData()
    {
        $htmPaginacion = [
            'resume' => '',
            'pages' => '',
        ];
        $listDatos = [];

        if (!$this->sqlQuery) {
            return [$htmPaginacion, $listDatos];
        }

        // Eloquent ---
        elseif ($this->sqlQuery instanceof \Illuminate\Pagination\LengthAwarePaginator) {
            $htmPaginacion = $this->getEloquentPagination($this->sqlQuery);
            return [$htmPaginacion, $this->sqlQuery];
        }
        elseif ($this->sqlQuery instanceof \Illuminate\Database\Eloquent\Builder) {
            $this->setEloquentData($this->sqlQuery);
            $htmPaginacion = $this->getEloquentPagination($this->sqlQuery);
            return [$htmPaginacion, $this->sqlQuery];
        }
        // Array ---
        elseif (is_array($this->sqlQuery)) {
            return [$htmPaginacion, $this->sqlQuery];
        }

        throw new \Exception("WList: Error Processing data type", 1);
    }
    //--------------------------------------------------------------
    private function getEloquentPagination(\Illuminate\Pagination\LengthAwarePaginator $data): ?array
    {
        // Prev link ----
        $previous_link = '#';
        $previous_disabled = ' disabled';
        if ($data->currentPage() > 1) {
            $previous_link = $this->paginationGetLink($data->currentPage() - 1);
            $previous_disabled = '';
        }

        // Pages ----
        $currentPage = $data->currentPage();

        // Show 10 pages max ------
        $iFirst = $currentPage - 5;
        $iLast  = $currentPage + 4;

        if ($iFirst < 1) {
            $iLast  = 10;
        }
        if ($iLast > $data->lastPage()) {
            $iFirst = $data->lastPage() - 9;
            $iLast = $data->lastPage();
        }
        if ($iFirst < 1) {
            $iFirst = 1;
        }

        // List pages ---
        $listPages = '';
        for ($i = $iFirst; $i <= $iLast; $i++) {

            $active = ($currentPage == $i) ? ' active' : '';
            $link = $this->paginationGetLink($i);

            $listPages .= "<li class='paginate_button page-item $active'><a href='$link' class='page-link'>$i</a></li>";
        }

        // Next link ----
        $next_link = '#';
        $next_disabled = ' disabled';
        if ($data->currentPage() != $data->lastPage()) {
            $next_link = $this->paginationGetLink($data->currentPage() + 1);
            $next_disabled = '';
        }

        // Resume ---
        $numTotal  = $data->total();
        $str_desde = $data->firstItem();
        $str_hasta = $data->lastItem();

        $strResume = Local::$t['Showing']." $str_desde ".Local::$t['to']." $str_hasta ".Local::$t['of']." <b>$numTotal</b>";

        // Pages ---
        $htmPages = <<<EOD
<li class="paginate_button page-item previous $previous_disabled"><a href="$previous_link" class='page-link'>&laquo;</a></li>
    $listPages
<li class="paginate_button page-item next $next_disabled"><a href="$next_link" class='page-link'>&raquo;</a></li>
EOD;

        return [
            'pages' => $htmPages,
            'resume' => $strResume
        ];
    }
    //--------------------------------------------------------------
    // For Eloquent data
    public function setEloquentData(\Illuminate\Database\Eloquent\Builder $data)
    {
        // Order ---
        $orderParams = $this->getOrderParams();
        $data->orderBy($orderParams[0], $orderParams[1]);

        // Pagination ---
        $page = EventStatus::getData($this->id_object, 'page');
        if (!$page) {
            $page = 1;
        }

        // Illuminate\Pagination\LengthAwarePaginator ---
        $this->sqlQuery = $data->paginate($this->paging_numRows, ['*'], 'page', $page);
    }
    //--------------------------------------------------------------
    private function paginationGetLink($page)
    {
        // dump($this->event_page);
        return str_replace('#page#', $page, $this->event_page);
    }
    //--------------------------------------------------------------
    private function getOrderParams(): array
    {
        $order_column = EventStatus::getData($this->id_object, 'order_column');
        if (!$order_column) {
            return ['id', 'asc'];
        }

        $order = EventStatus::getData($this->id_object, 'order');
        if (!$order) {
            $order = 'asc';
        }

        return [$order_column, $order];
    }
    //--------------------------------------------------------------
}