<?php
/**
 * @author José A. Romero Vegas <jangel.romero@gmail.com>
 *
 */

namespace angelrove\CrudCore;

// use angelrove\membrillo\WApp\Session;
// use angelrove\CrudCore\FormInputs\FormInputs;
// use angelrove\CssJsLoad\CssJsLoad;

class Local
{
    public static $t = array();

    private static $cookieName = 'cmsLang';
    private static $acceptLang = ['es', 'en'];

    //------------------------------------------------------
    public static function _init(): void
    {
        // Default ----
        if (!self::getLang()) {
            self::setLang(self::getBrowserLang());
        }

        self::loadLangFiles();
    }
    //------------------------------------------------------
    public static function _init_sections(): void
    {
        self::loadLangFilesSection();
    }
    //------------------------------------------------------
    private static function loadLangFiles(): void
    {
        $dir_lang = 'local_t/'.self::getLang().'.inc';

        include_once __DIR__.'/'.$dir_lang;
        // include_once PATH_SRC.'/'.$dir_lang;
    }
    //------------------------------------------------------
    private static function loadLangFilesSection(): void
    {
        global $CONFIG_SECCIONES, $seccCtrl;
        $secc_folder = $CONFIG_SECCIONES->getFolder($seccCtrl->secc);

        $dir_lang = 'local_t/'.self::getLang().'.inc';
        if (file_exists($secc_folder.'/'.$dir_lang)) {
            include_once $secc_folder.'/'.$dir_lang;
        }
    }
    //------------------------------------------------------
    public static function onChangeLang(): void
    {
        self::setLang($_GET['val']);

        // Reload "CONFIG_SECC"
        // Session::unset('CONFIG_SECCIONES');
    }
    //------------------------------------------------------
    public static function setLang(string $lang): void
    {
        setcookie(self::$cookieName, $lang, time()+60*60*24*60, '/');
        $_COOKIE[self::$cookieName] = $lang;
    }
    //------------------------------------------------------
    public static function getLang(): ?string
    {
        return $_COOKIE[self::$cookieName]?? null;
    }
    //------------------------------------------------------
    public static function getLangLabel(): string
    {
        $lang = self::getLang();
        return (!$lang || $lang=='es')? '' : '_'.$lang;
    }
    //------------------------------------------------------
    // PRIVATE
    //------------------------------------------------------
    private static function getBrowserLang(): string
    {
        $lang = '';

        if (isset($_SERVER['HTTP_ACCEPT_LANGUAGE'])) {
            $lang = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
        } else {
            return 'en';
        }

        return (in_array($lang, self::$acceptLang))? $lang : 'en';
    }
    //------------------------------------------------------
}
