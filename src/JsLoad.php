<?php
/**
 * @author José A. Romero Vegas <jangel.romero@gmail.com>
 * 2011
 *
 */

namespace angelrove\CrudCore;

class JsLoad
{
    use CssJsLoader\LoaderTrait;

    private static $list_jsVars = [];

    //---------------------------------------------------------------------
    public static function addVars(array $jsVars): void
    {
        self::check_get_calls();
        self::$list_jsVars += $jsVars;
    }
    //---------------------------------------------------------------------
    public static function get(): void
    {
        self::$called_get = true;

        echo '<!-- Imported with angelrove\JsLoad -->' . PHP_EOL .
        self::importFilesHttp().
        self::import_jsVars(self::$list_jsVars) .
        self::importFiles();
    }
    //---------------------------------------------------------------------
    private static function importFilesHttp(): string
    {
        $strImports = '';
        foreach (self::$list_files_http as $file) {
            $strImports .= '<script src="' . $file . '"></script>' . PHP_EOL;
        }
        return $strImports;
    }

    private static function importFiles(): string
    {
        $strImports = '';
        foreach (self::$list_files as $file) {
            $strImports .= '<script type="module" src="' . asset($file) . '"></script>' . PHP_EOL;
        }
        return $strImports;
    }
    //---------------------------------------------------------------------
    private static function import_jsVars($jsVars): string
    {
        if (!$jsVars) {
            return '';
        }

        $strVars = '';
        foreach($jsVars as $key => $value) {
            $value = (is_numeric($value))? $value : "'$value'";
            $strVars .= "var $key = $value;\n";
        }

        return PHP_EOL .
            '<script>' . PHP_EOL .trim($strVars) . PHP_EOL . '</script>' .
            PHP_EOL;
    }
    //---------------------------------------------------------------------
}
