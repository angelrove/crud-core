<?php
/**
 * FormInputs
 *
 * @author José A. Romero Vegas <jangel.romero@gmail.com>
 */

namespace angelrove\CrudCore\FormInputs;

abstract class Component
{
    protected $name;
    protected $value;
    protected $required = false;
    protected $placeholder = false;
    protected $readonly = false;
    protected $autoSubmit = false;
    protected $htmlAttributes = '';

    protected $title;
    protected $container;

    abstract protected function getComponent(): string;

    //-------------------------------------------------------
    public function __construct(string $name = '', $value = '')
    {
        $this->name = $name;
        $this->value = $value;
    }
    //-------------------------------------------------------
    public function get(): string
    {
        if ($this->container) {
            if ($this->required) {
                $this->title .= ' *';
            }
            return InputContainer::{$this->container}($this->getComponent(), $this->title, $this->name);
        } else {
            return $this->getComponent();
        }
    }
    //-------------------------------------------------------
    // Properties
    //-------------------------------------------------------
    public function title(string $title): Component
    {
        $this->title = $title;
        return $this;
    }

    public function value(string $value): Component
    {
        $this->value = $value;
        return $this;
    }

    public function container(string $container = 'basic'): Component
    {
        $this->container = $container;
        return $this;
    }

    public function required(bool $required = true): Component
    {
        $this->required = $required;
        return $this;
    }

    public function readonly(bool $readonly = true): Component
    {
        $this->readonly = $readonly;
        return $this;
    }

    public function placeholder(string $placeholder = ''): Component
    {
        $this->placeholder = ($placeholder)? $placeholder : true;
        return $this;
    }

    public function autoSubmit(): Component
    {
        $this->autoSubmit = true;
        return $this;
    }

    public function attributes(string $htmlAttributes): Component
    {
        $this->htmlAttributes .= $htmlAttributes;
        return $this;
    }
    //------------------------------------------------------------------
    protected function helperGetAutoInput(string $type): string
    {
        // Readonly ---
        if ($this->readonly) {
            $this->htmlAttributes .= ' disabled ';
            $this->name = '';
        }

        // Required ---
        if ($this->required) {
            $this->htmlAttributes .= ' required';
        }

        // Placeholder ---
        if ($this->placeholder === true) {
            $this->htmlAttributes .= ' placeholder="' . $this->title . '"';
        } else if ($this->placeholder) {
            $this->htmlAttributes .= ' placeholder="' . $this->placeholder . '"';
        }

        // HTML -------
        if ($type == 'textarea') {
            $out = '<textarea class="form-control type_'.$type.'"'.
                       ' ' . $this->htmlAttributes .
                       ' name="' . $this->name . '"'.
                    '>'.$this->value.'</textarea>';
        } else {
            $out = '<input class="form-control type_'.$type.'"'.
                       ' ' . $this->htmlAttributes .
                       ' type="'  . $type  . '"'.
                       ' name="'  . $this->name  . '"'.
                       ' value="' . $this->value . '"'.
                   '> ';
        }

        return $out;
    }
    //------------------------------------------------------------------
}
