<?php
/**
 *
 * @author José A. Romero Vegas <jangel.romero@gmail.com>
 */

namespace angelrove\CrudCore\FormInputs;

use angelrove\CrudCore\FormInputs\Component;

class GenericComponent extends Component
{
    protected $type;

    public function __construct(string $type, string $name = '', $value = '')
    {
        parent::__construct($name, $value);
        $this->type = $type;
    }

    protected function getComponent(): string
    {
        return parent::helperGetAutoInput($this->type);
    }
}
