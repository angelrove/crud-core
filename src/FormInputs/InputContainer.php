<?php
namespace angelrove\CrudCore\FormInputs;

class InputContainer
{
    public static function basic(string $strInput, ?string $title, string $name = ''): string
    {
        return <<<EOD
        <div class="form-group form-group col-sm-12" element="div" id="$name">
            <label>$title</label> $strInput
        </div>
        EOD;
    }

    public static function inline(string $strInput, ?string $title, string $name = ''): string
    {
        return <<<EOD
        <div class="form-group" id="$name">
            <label for="$name" >$title</label> $strInput
        </div>
        EOD;
    }

    public static function horizontal(string $strInput, ?string $title, string $name = ''): string
    {
        return <<<EOD
        <div class="form-group col-sm-12" element="div" id="$name">
            <label for="$name">$title</label>
            <div class="col-sm-12">
                $strInput
            </div>
        </div>
        EOD;
    }

    public static function start(?string $title, string $name = ''): string
    {
        return <<<EOD
        <div class="form-group col-sm-12" element="div" id="$name">
            <label for="$name">$title</label>
            <div class="col-sm-9">
        EOD;
    }

    public static function end(): string
    {
        return '
            </div>
        </div>
        ';
    }
}
