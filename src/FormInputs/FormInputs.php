<?php
/**
 * FormInputs
 *
 * @author José A. Romero Vegas <jangel.romero@gmail.com>
 */

namespace angelrove\CrudCore\FormInputs;

use angelrove\CrudCore\FormInputs\Components\Select;
use angelrove\CrudCore\FormInputs\Components\Radios;
use angelrove\CrudCore\FormInputs\Components\Check;
use angelrove\CrudCore\FormInputs\Components\File\File;
use angelrove\CrudCore\FormInputs\Components\Percent;
use angelrove\CrudCore\FormInputs\Components\Datetime;
use angelrove\CrudCore\FormInputs\Components\Url;
use angelrove\CrudCore\FormInputs\Components\Autocomplete\Autocomplete;

class FormInputs
{
    /* Simple method to get component (using laravel/collective) */
    public static function getInput($type, $name, $value, $required=null, $disabled=null, $rows=null)
    {
        $inputHtm = '';
        switch ($type) {
            case 'url':
            case 'price':
            case 'percent':
                $data = \Form::getModel();
                $inputHtm = FormInputs::{$type}($name, $data[$name])
                    ->required($required)
                    ->get();
            break;

            case 'autocomplete':
                $data = \Form::getModel();
                $inputHtm = FormInputs::autocomplete($name, $data[$name])
                    ->required($required)
                    ->urlAjax(route('fundings.autocomplete'))
                    ->get();
            break;

            default:
                $class = 'form-control ';
                $class .= match($type) {
                    // 'date'   => 'w-25',
                    'number' => 'w-25',
                    'tel'    => 'w-25',
                    default  => '',
                };

                $options = [
                    'class' => $class,
                    'required' => $required,
                    'disabled' => $disabled,
                    'rows' => $rows,
                ];

                $inputHtm = \Form::{$type}($name, $value, $options);
        }

        return $inputHtm;
    }

    /*
     * Components
     */

    public static function text(string $name = '', $value = ''): GenericComponent
    {
        return (new GenericComponent('text', $name, $value));
    }

    public static function password(string $name = '', $value = ''): GenericComponent
    {
        return (new GenericComponent('password', $name, $value));
    }

    public static function textarea(string $name = '', $value = ''): GenericComponent
    {
        return (new GenericComponent('textarea', $name, $value));
    }

    public static function hidden(string $name = '', $value = ''): GenericComponent
    {
        return (new GenericComponent('hidden', $name, $value));
    }

    public static function select(string $name = '', $value = ''): Select
    {
        return new Select($name, $value);
    }

    public static function radios(string $name = '', $value = ''): Radios
    {
        return new Radios($name, $value);
    }

    public static function check(string $name = '', $value = ''): Check
    {
        return new Check($name, $value);
    }

    public static function file(string $name = '', $value = ''): File
    {
        return new File($name, $value);
    }

    public static function email(string $name = '', $value = ''): GenericComponent
    {
        return (new GenericComponent('email', $name, $value));
    }

    public static function number(string $name = '', $value = ''): GenericComponent
    {
        if (!$value) {
            $value = '0';
        }
        return (new GenericComponent('number', $name, $value))
            ->attributes(' style="width:initial"');
    }

    public static function price(string $name = '', $value = ''): GenericComponent
    {
        if (!$value) {
            $value = '0.00';
        }
        return (new GenericComponent('number', $name, $value))
            ->attributes(' style="width:initial"')
            ->attributes(' min="0" step=".01"');
    }

    public static function percent(string $name = '', $value = ''): Percent
    {
        return new Percent($name, $value);
    }

    public static function datetime(string $name = '', $value = ''): Datetime
    {
        return new Datetime($name, $value);
    }

    public static function date(string $name = '', $value = ''): GenericComponent
    {
        return (new GenericComponent('date', $name, $value))
            ->attributes('style="width:initial"');
    }

    public static function month(string $name = '', $value = ''): GenericComponent
    {
        return (new GenericComponent('month', $name, $value))
            ->attributes('style="width:initial"');
    }

    public static function week(string $name = '', $value = ''): GenericComponent
    {
        return (new GenericComponent('week', $name, $value))
            ->attributes('style="width:initial"');
    }

    public static function time(string $name = '', $value = ''): GenericComponent
    {
        return (new GenericComponent('time', $name, $value))
            ->attributes('style="width:initial"');
    }

    public static function url(string $name = '', $value = ''): Url
    {
        return new Url($name, $value);
    }

    public static function tel(string $name = '', $value = ''): GenericComponent
    {
        return (new GenericComponent('tel', $name, $value))
            ->attributes('style="width:initial"');
    }

    public static function autocomplete(string $name, ?string $value = '', ?string $value_label = '', string $url_ajax = ''): Autocomplete
    {
        return new Autocomplete($name, $value, $value_label, $url_ajax);
    }
}
