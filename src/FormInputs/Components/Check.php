<?php
/**
 *
 * @author José A. Romero Vegas <jangel.romero@gmail.com>
 */

namespace angelrove\CrudCore\FormInputs\Components;

use angelrove\CrudCore\FormInputs\Component;

class Check extends Component
{
    private $label = '';

    /**
     * $value = por defecto admite los valores: 0, 1
     */
    protected function getComponent(): string
    {
        $setValueChecked = '1';

        $value = $this->value;
        if ($this->value) {
            $this->htmlAttributes .= ' checked ';
        } else {
            $value = 0;
        }

        // Readonly ---
        if ($this->readonly) {
            $this->htmlAttributes .= ' disabled ';
            $this->name = '';
        }

        // Required ---
        if ($this->required) {
            $this->htmlAttributes .= ' required';
        }

        return <<<EOD
    <label class="inputCheck" for="InputCheck_$this->name">
        <input type="hidden" name="$this->name" value="$value">
        <input type="checkbox" id="InputCheck_$this->name" class="big-checkbox" value="$setValueChecked" $this->htmlAttributes>
        $this->label
    </label>

    <script>
    document.querySelector('#InputCheck_$this->name').onclick = function()
    {
       let theValue = (this.checked)? '$setValueChecked' : '0';
       document.querySelector("input[name='$this->name']").value = theValue;
    }
    </script>
EOD;
    }

    public function label(string $label): Component
    {
        $this->label = $label;
        return $this;
    }
}
