<?php
/**
 *
 * @author José A. Romero Vegas <jangel.romero@gmail.com>
 */

namespace angelrove\CrudCore\FormInputs\Components;

use angelrove\CrudCore\FormInputs\Component;

class Datetime extends Component
{
    private $timezone;

    protected function getComponent(): string
    {
        $this->value = self::parseValue($this->value, $this->timezone);
        return $this->helperGetAutoInput('datetime-local');
    }

    public function timezone(?string $timezone)
    {
        $this->timezone = $timezone;
        return $this;
    }

    /**
     * @param $value mixed [int timestamp, Carbon object, string datetime]
     */
    private static function parseValue($value, string $timezone = null): string
    {
        if ($value instanceof \Illuminate\Support\Carbon) {
        } else {
            $value = \Illuminate\Support\Carbon::parse($value);
        }

        return $value->timezone($timezone)->format('Y-m-d\TH:i');
    }
    //---------------------------------------------------
    // DB insert helper
    //---------------------------------------------------
    public static function dateTimeToTimestamp($dateTime)
    {
        $time = false;

        // 2018-01-01T22:02 -------
        if ($date = \DateTime::createFromFormat('Y-m-d\TH:i', $dateTime)) {
        }
        // 2018-01-01T22:02:00 -------
        elseif ($date = \DateTime::createFromFormat('Y-m-d\TH:i:s', $dateTime)) {
        }
        // 2018-01-01 22:02:00 -------
        elseif ($date = \DateTime::createFromFormat('Y-m-d H:i:s', $dateTime)) {
        }

        if ($date) {
            return $date->getTimestamp();
        } else {
            throw new \Exception("FormInputs >> dateTimeToTimestamp(): Error processing date!! [$dateTime]");
        }

        return $time;
    }
}
