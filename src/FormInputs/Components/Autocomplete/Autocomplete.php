<?php
/**
 * @author José A. Romero Vegas <jangel.romero@gmail.com>
 *
 */

namespace angelrove\CrudCore\FormInputs\Components\Autocomplete;

use angelrove\CrudCore\JsLoad;
use angelrove\CrudCore\CrudCore;
use angelrove\CrudCore\FormInputs\Component;

class Autocomplete extends Component
{
    private $url_ajax;

    //---------------------------------------------------------------------
    public function __construct(string $name, ?string $value = '', ?string $value_label = '')
    {
        parent::__construct($name, $value);

        $this->input_label = $name.'_label';
        $this->value_label = $value_label;
    }
    //---------------------------------------------------------------------
    public function setValueLabel(?string $value_label)
    {
        $this->value_label = $value_label;
        return $this;
    }
    //---------------------------------------------------------------------
    public function urlAjax(string $url_ajax)
    {
        $this->url_ajax = $url_ajax;
        return $this;
    }
    //---------------------------------------------------------------------
    protected function getComponent(): string
    {
        JsLoad::addVars([
           'AUTOCOMPLETE_INPUT_ID'   => $this->name,
           'AUTOCOMPLETE_INPUT_NAME' => $this->name,
           'AUTOCOMPLETE_URL_AJAX'   => $this->url_ajax,
        ]);

        JsLoad::add(CrudCore::VENDOR_DIR.'/FormInputs/autocomplete.js');

        // Required ---
        if ($this->required) {
            $this->htmlAttributes .= ' required';
        }

      return '
      <input type="text" class="form-control" name="'.$this->name.'" value="'.$this->value.'" '.$this->htmlAttributes.'>
      ';
    }
    //---------------------------------------------------------------------
}
