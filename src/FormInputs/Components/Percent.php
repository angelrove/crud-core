<?php
/**
 *
 * @author José A. Romero Vegas <jangel.romero@gmail.com>
 */

namespace angelrove\CrudCore\FormInputs\Components;

use angelrove\CrudCore\FormInputs\Component;

class Percent extends Component
{
    protected function getComponent(): string
    {
        $this->htmlAttributes .= ' style="width:initial"';
        $this->htmlAttributes .= ' min="0" max="100" step=".01"';

        if ($this->title) {
            $this->title .= ' (%)';
        }

        if (!$this->value) {
            $this->value = '0.00';
        }

        return $this->helperGetAutoInput('number');
    }
}
