<?php
/**
 *
 * @author José A. Romero Vegas <jangel.romero@gmail.com>
 */

namespace angelrove\CrudCore\FormInputs\Components;

use angelrove\CrudCore\FormInputs\Component;

class Radios extends Component
{
    private $data = [];
    private $listColors = [];
    private $listGroup;
    private $is_assoc = false;

    public function data(array $data)
    {
        $this->data = $data;
        return $this;
    }

    public function colors(array $listColors)
    {
        $this->listColors = $listColors;
        return $this;
    }

    protected function getComponent(): string
    {
        $strSelect = '';

        $required = ($this->required) ? 'required' : '';

        // ¿Viene con claves?
        $isAsociativo = $this->is_assoc;
        foreach ($this->data as $key => $row) {
            // Data ---
            $optionId = '';
            $optionLabel = '';

            if (is_array($row)) {
                $optionId    = $row['id'];
                $optionLabel = $row['name'];
                // $optionLabel = ($row['name'])?? $id;
            } elseif (is_object($row)) {
                $optionId    = $row->id;
                $optionLabel = $row->name;
            } else {
                $optionId    = $key;
                $optionLabel = $row;
            }

            // Selected
            $SELECTED = '';
            if (strcmp($optionId, $this->value) == 0) {
                $SELECTED = ' checked';
            }

            $idCheck = $this->name . '_' . $optionId;

            // Color
            $style_bg = '';

            // Option
            $strSelect .= <<<EOD
                 <label class="radio-inline" $style_bg>
                    <input type="radio" $required
                        id="$idCheck"
                        name="$this->name"
                        value="$optionId" $SELECTED>$optionLabel
                 </label>\n
                 EOD;
        }

        return <<<EOD
        <div>
            $strSelect
        </div>
        EOD;
    }

    /* With images  */
    public function show2($id_check)
    {
        $strSelect = '';

        // Required ---
        if ($this->required) {
            $required = ' required';
        }

        // ¿Viene con claves?
        $isAsociativo = ($this->data[0]) ? false : true;

        $nameCheck = $this->name . '[' . $id_check . ']';
        $idCheck   = $this->name . '_' . $id_check . '_';

        foreach ($this->data as $id => $image) {
            // Selected
            $SELECTED = '';
            if ($id == $this->value) {
                $SELECTED = ' checked';
            }

            $idCheck .= $id;

            // Option
            $strSelect .= <<<EOD
          <label class="radio-inline">
            <input type="radio" $required
                   id="$idCheck"
                   name="$nameCheck"
                   value="$id" $SELECTED>
            $image
          </label>
EOD;
        }

        return
            '<div '.$required.' class="WInputRadios_img" id="WInputRadios_img_'.$this->name.'">'
                .$strSelect.
            '</div>';
    }
    //----------------------------------------------------------------------
}
