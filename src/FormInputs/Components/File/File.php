<?php
/**
 * File: HTM input file object
 * FileUpload: get file uploaded
 *
 * @author José A. Romero Vegas <jangel.romero@gmail.com>
 */

namespace angelrove\CrudCore\FormInputs\Components\File;

use angelrove\CrudCore\FormInputs\Component;
use angelrove\utils\FileUploaded;
// use angelrove\utils\FileContent;
use angelrove\CrudCore\JsLoad;
use angelrove\CrudCore\CrudCore;

class File extends Component
{
    private $showDel      = true;
    private $showFileName = false;
    private $showImg      = false;
    private $showImgWidth = 100;

    private $validFiles   = '';
    private $saveAs       = '';
    private $setMaxSize   = 0;
    private $setMaxWidth    = 0;
    private $setMaxHeight   = 0;
    private $setCropHeight  = 0;
    private $setCropHeightY = '';
    private $setThumbWidth  = 0;
    private $watermark_text = '';

    //---------------------------------------------------------------------
    public function __construct($name, $value)
    {
        parent::__construct($name, $value);

        JsLoad::add(CrudCore::VENDOR_DIR . '/assets/file.js');

        // Membrillo
        if (defined('CACHE_PATH')) {
            \angelrove\utils\CssJsLoad::set(__DIR__ . '/../../assets/file.js');
        }
    }
    //---------------------------------------------------------------------
    public function hiddenBtDelete()
    {
        $this->showDel = false;
        return $this;
    }
    //---------------------------------------------------------------------
    // $flag: false, true, short
    public function showFileName(bool $flag)
    {
        $this->showFileName = $flag;
        return $this;
    }
    //---------------------------------------------------------------------
    public function showImg(bool $flag, $width, $height = '')
    {
        $this->showImg       = $flag;
        $this->showImgWidth  = $width;
        $this->showImgHeight = $height;
        return $this;
    }
    //---------------------------------------------------------------------
    // Propiedades del archivo
    //---------------------------------------------------------------------
    public function validFiles($listMimes)
    {
        $this->validFiles = $listMimes;
        return $this;
    }
    //---------------------------------------------------------------------
    /*
     *  $saveAs: - prefijo que se añade al nombre formateado,
     *           - KEEP_NAME: para mantener el nombre original
     */
    public function saveAsName($saveAs)
    {
        $this->saveAs = $saveAs;
        return $this;
    }
    //---------------------------------------------------------------------
    public function maxSize($setMax)
    {
        $this->setMaxSize = $setMax;
        return $this;
    }
    //---------------------------------------------------------------------
    // Imagenes
    //---------------------------------------------------------------------
    public function img_maxWidth($setMaxWidth)
    {
        $this->setMaxWidth = $setMaxWidth;
        return $this;
    }
    //---------------------------------------------------------------------
    public function img_maxHeight($setMaxHeight)
    {
        $this->setMaxHeight = $setMaxHeight;
        return $this;
    }
    //---------------------------------------------------------------------
    public function img_cropHeight($setCropHeight, string $cropY = 'bottom')
    {
        $this->setCropHeight  = $setCropHeight;
        $this->setCropHeightY = $cropY;
        return $this;
    }
    //---------------------------------------------------------------------
    public function img_thumbWidth($setThumbWidth)
    {
        $this->setThumbWidth = $setThumbWidth;
        return $this;
    }
    //---------------------------------------------------------------------
    public function set_watermark(string $text)
    {
        $this->watermark_text = $text;
        return $this;
    }
    //---------------------------------------------------------------------
    // GET
    //---------------------------------------------------------------------
    protected function getComponent(): string
    {
        $htmFilePrev  = '';
        $bt_delete    = '';
        $displayInput = '';

        if ($this->value) {
            /** File prev. **/
            $htmFilePrev = $this->getHtm_fileInfo();

            /** Ocultar "input file" **/
            $displayInput = 'style="display:none"';
        }

        /** Read only **/
        if ($this->readonly === true) {
            if (!$htmFilePrev) {
                $htmFilePrev = '<input type="text" disabled value="">';
            }
            return <<<EOD
         <!-- WInputFile -->
         <div class="well well-sm display-table strip-margin">
            <div class="WInputFile" id="'.$this->name.'_htmFilePrev" class="prevFile">$htmFilePrev</div>
         </div>
         <!-- /WInputFile -->
EOD;
        }

        /** HTM **/
        // NOTA: Los parámetros que configuran el upload se pasan por POST con hidden.
        //       Este sistema no es seguro. Sería mejor utilizar sesiones ("$objectsStatus->setDato()")
        if ($htmFilePrev) {
            $htmFilePrev = '<td id="' . $this->name . '_htmFilePrev" class="prevFile">' . $htmFilePrev . '</td>';
        }

        // Title
        $htmLabel = '';
        // if ($this->title) {
        //     $htmLabel = '<td>' . $this->title . '&nbsp; </td>';
        // }

        $required = '';
        // if ($this->required == true && !$htmFilePrev) {
        if ($this->required == true) {
            $required = 'required';
        }

        return '
<!-- ----- WInputFile ----- -->
<input type="hidden" name="' . $this->name . '_isDelete"        value="0">
<input type="hidden" name="' . $this->name . '_prev"            value="' . $this->value . '">
<input type="hidden" name="' . $this->name . '_saveAs"          value="' . $this->saveAs . '">
<input type="hidden" name="' . $this->name . '_validFiles"      value="' . $this->validFiles . '">
<input type="hidden" name="' . $this->name . '_setMaxSize"      value="' . $this->setMaxSize . '">
<input type="hidden" name="' . $this->name . '_setMaxWidth"     value="' . $this->setMaxWidth . '">
<input type="hidden" name="' . $this->name . '_setMaxHeight"    value="' . $this->setMaxHeight . '">
<input type="hidden" name="' . $this->name . '_setCropHeight"   value="' . $this->setCropHeight . '">
<input type="hidden" name="' . $this->name . '_setCropHeightY"  value="' . $this->setCropHeightY . '">
<input type="hidden" name="' . $this->name . '_setThumbWidth"   value="' . $this->setThumbWidth . '">
<input type="hidden" name="' . $this->name . '_watermark_text"  value="' . $this->watermark_text . '">

<div class="WInputFile well well-sm display-table strip-margin">
  <table><tr>
    ' . $htmLabel . $htmFilePrev . '
    <td id="' . $this->name . '_obj_input" ' . $displayInput . '>
       <input type="file" id="' . $this->name . '" name="' . $this->name . '" ' . $required . ' size="27">
    </td>
  </tr></table>
</div>
<!-- ----- /WInputFile ----- -->
';
    }
    //---------------------------------------------------------------------
    // PRIVATE
    //---------------------------------------------------------------------
    private function btDelete()
    {
        return <<<EOD
        <button type="button" param_input_name="$this->name"
                class="WInputFile_del btn btn-default btn-sm">
           <i class="fas fa-trash-alt fa-2x"></i>
        </button>
        EOD;
    }
    //---------------------------------------------------------------------
    // A partir de la extensión
    private function getTypeFile(string $file): string
    {
        if (substr($file, 0, 4) == 'http') {
            return 'IMAGE_LINK';
        }

        $ext = substr($file, -4, 4);
        //echo "file='$file'; ext=".$ext."<br />";

        switch (strtoupper($ext)) {
            case '.JPG':
            case 'JPEG':
            case '.GIF':
            case '.PNG':
                return 'IMAGE';
                break;
            default:
                return 'FILE';
                break;
        }
    }
    //---------------------------------------------------------------------
    private function get_fileInfo(array $datosFile): string
    {
        $labelFileInfo = '';

        //-------
        if ($datosFile['fecha'] && $datosFile['size']) {
            $lb_ruta     = $datosFile['ruta_completa'];
            $lb_nameUser = '[' . $datosFile['nameUser'] . ']';
            $lb_fecha    = ' [' . $datosFile['fecha'] . '] ';
            $lb_size     = round(($datosFile['size'] / 1024), 1) . 'k';

            $labelFileInfo = $lb_ruta . '<br>' . $lb_nameUser . $lb_fecha . $lb_size;
        } else {
            // Compatibilidad
            $labelFileInfo = $datosFile['nameUser'];
        }

        // short -------
        if ($this->showFileName === 'short') {
            $labelFileInfo = $lb_nameUser;
        }

        return '<div class="file_info well-sm">' . $labelFileInfo . '</div>';
    }
    //---------------------------------------------------------------------
    private function getHtm_fileInfo(): string
    {
        global $CONFIG_APP, $seccCtrl;

        /* Datos file */
        $datosFile = FileUploaded::getInfo($this->value, $seccCtrl->UPLOADS_DIR_DEFAULT);
        if (!$datosFile['nameUser']) {
            $datosFile['nameUser'] = $datosFile['name'];
        }

        /* Out */
        // View -------
        $fileProp_TYPE = $this->getTypeFile($datosFile['name']); // IMAGE, FILE
        $fileProp_URL  = $datosFile['ruta_completa'];

        $linkView = '';
        if ($fileProp_TYPE == 'IMAGE') {
            $linkView = '<div class="view_image">' . FileUploaded::getHtmlImg($datosFile, 'lightbox', '', '', true) . '</div>';
        } elseif ($fileProp_TYPE == 'IMAGE_LINK') {
            $linkView = '<div class="view_image">' . FileUploaded::getHtmlImg($datosFile, 'lightbox', '', '', true) . '</div>';
        } elseif ($datosFile['mime'] == 'application/pdf' || $datosFile['mime'] == 'text/plain') {
            // Open: "pdf" and "txt" ---
            $linkView = '<a class="img-thumbnail" href="' . $fileProp_URL . '" target="_blank">' .
                '<i class="fas fa-file-pdf fa-4x" aria-hidden="true"></i>' .
                '</a>';
        } elseif (!$datosFile['mime']) {
            // Open: if not a MIME Type ---
            $linkView = '<a class="img-thumbnail" href="' . $fileProp_URL . '" target="_blank">' .
                '<i class="fas fa-file-alt fa-4x"></i>' .
                '</a>';
        }

        // Info --------
        $labelFileInfo = '';
        if ($this->showFileName) {
            $labelFileInfo = $this->get_fileInfo($datosFile);
        }

        // Download ---
        $linkDownload =
           '<a class="btn btn-default btn-sm" href="' . $fileProp_URL . '" target="_blank" download>' .
              '<i class="fas fa-download fa-2x" aria-hidden="true"></i>' .
           '</a>';

        // Delete -----
        $bt_delete = '';
        if (!$this->readonly && $this->showDel === true) {
            $bt_delete = $this->btDelete();
        }

        return '
      <!-- File info -->
      ' . $labelFileInfo . '
      <div class="text-center">' . $linkView . '</div>
      <div class="text-center">' . $linkDownload . ' ' . $bt_delete . '</div>
      <!-- /File info -->
    ';
    }
    //---------------------------------------------------------------------
}
