<?php
/**
 *
 * @author José A. Romero Vegas <jangel.romero@gmail.com>
 */

namespace angelrove\CrudCore\FormInputs\Components;

use angelrove\CrudCore\FormInputs\Component;

class Url extends Component
{
    protected function getComponent(): string
    {
        $addToInput = '';
        if ($this->value) {
            $addToInput =
                '<a target="_blank" href="'.$this->value.'" style="position: absolute; right: 27px; bottom: 5px;">'.
                    '<i class="fas fa-external-link-alt" ></i>'.
                '</a> ';
        }

        // $this->htmlAttributes .= ' style="" ';

        return '<div>'.$this->helperGetAutoInput('url').$addToInput.'</div>';
    }
}
