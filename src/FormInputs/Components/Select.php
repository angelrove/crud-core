<?php
/**
 *
 * @author José A. Romero Vegas <jangel.romero@gmail.com>
 */

namespace angelrove\CrudCore\FormInputs\Components;

use angelrove\CrudCore\FormInputs\Component;
use angelrove\utils\Db_mysql;

class Select extends Component
{
    private $data = [];
    private $listColors;
    private $listGroup;
    private $labelAsValue = false;

    protected function getComponent(): string
    {
        // Data in SQL format -----
        if (is_string($this->data)) {
            $this->data = Db_mysql::getList($this->data);
        }

        $htmOptions = $this->getOptions($this->data, $this->value);

        return self::htmSelect($htmOptions);
    }
    //--------------------------------------------------------------
    public function labelAsValue()
    {
        $this->labelAsValue = true;
    }
    //--------------------------------------------------------------
    private function getOptions(array $data, $value)
    {
        $htmOptions = '';

        foreach ($data as $key => $row)
        {
            // id / value ---
            $optionId    = '';
            $optionLabel = '';

            if (is_array($row)) {
                $optionId    = $row['id'];
                $optionLabel = $row['name'];
                // $optionLabel = ($row['name'])?? $id;
            } elseif (is_object($row)) {
                $optionId    = $row->id;
                $optionLabel = $row->name;
            } else {
                $optionId    = $key;
                $optionLabel = $row;
            }

            if ($this->labelAsValue) {
                $optionId = $optionLabel;
            }

            // Selected ---
            $SELECTED = '';
            if (is_array($value)) {
                if (array_search($optionId, (array) $value) !== false) {
                    $SELECTED = 'SELECTED';
                }
            } else {
                if ($optionId == $value) {
                    $SELECTED = 'SELECTED';
                }
            }

            // optgroup ---
            if ($this->listGroup && isset($this->listGroup[$optionId])) {
                if ($htmOptions) {
                    $htmOptions .= '</optgroup>';
                }
                $htmOptions .= '<optgroup label="' . $this->listGroup[$optionId] . '">';
            }

            // Colors ---
            $style = '';
            if ($this->listColors) {
                $style = 'style="background:' . $this->listColors[$optionId] . '"';
            }

            // Option ---
            $htmOptions .= "<option $style value=\"$optionId\" $SELECTED>$optionLabel</option>";
        }

        if ($this->listGroup) {
            $htmOptions .= '</optgroup>';
        }

        return $htmOptions;
    }
    //--------------------------------------------------------------
    private function htmSelect($htmOptions)
    {
        // Readonly ---
        if ($this->readonly) {
            $this->htmlAttributes .= ' disabled ';
            $this->name = '';
        }

        // Required ---
        if ($this->required) {
            $this->htmlAttributes .= ' required';
        }

        // Placeholder ---
        $optionPlaceholder = '';
        if ($this->placeholder) {
            $label = $this->placeholder;
            $value = '';

            if ($this->placeholder === 'NULL') {
                $label = '-';
                $value = 'NULL';
            } else if ($this->placeholder === true) {
                $label = '-';
            }

            $optionPlaceholder = '<option value="'.$value.'" class="placeholder">-- '.$label.' --</option>';
        }

        // Auto submit ---
        $autoSubmit = '';
        if ($this->autoSubmit) {
            $autoSubmit = 'onchange="this.form.submit()"';
        }

        // Selector ------
        return
        "<select name=\"$this->name\" class=\"form-control\" $autoSubmit $this->htmlAttributes >" .
            $optionPlaceholder .
            $htmOptions .
        "</select>";
    }
    //--------------------------------------------------------------
    public function data($data)
    {
        $this->data = $data;
        return $this;
    }

    public function colors(array $listColors)
    {
        $this->listColors = $listColors;
        return $this;
    }

    public function groups(array $listGroup)
    {
        $this->listGroup = $listGroup;
        return $this;
    }
    //-------------------------------------------------------------
}
