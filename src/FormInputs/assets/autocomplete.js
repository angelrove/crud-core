
$(function() {
   $('[name="'+AUTOCOMPLETE_INPUT_NAME+'"]').autocomplete({
         minLength: 3,
         source: function( request, response ) {
           $.ajax({
             url: AUTOCOMPLETE_URL_AJAX,
             dataType: "json",
             data: {
                q: request.term
             },
             success: function( data ) {
                response( data );
             }
           });
         },
         select: function(e, ui) {
            $('[name="'+AUTOCOMPLETE_INPUT_ID+'"]').val(ui.item.value);

            // Listener: se llama a 'autocompleteOnChange' si existe
            if (typeof autocompleteOnChange === "function") {
               autocompleteOnChange(AUTOCOMPLETE_INPUT_ID, ui.item.value);
            }
         },
   });
});
