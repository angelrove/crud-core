
/**
 * On delete file
 */
document.querySelector('button.WInputFile_del').onclick = function()
{
    // Get attr "param_input_name" ---
    let WInputFile_name = this.getAttribute('param_input_name');

    // Mark deleted -----------
    document.querySelector("input[name='"+ WInputFile_name +"_isDelete']").value = '1';

    // Elementos visuales ------
    // Hide "Delete" button
    this.style.display = 'none';

    // Hide: button "View file"
    if (document.querySelector("#"+WInputFile_name+"_htmFilePrev")) {
        document.querySelector("#"+WInputFile_name+"_htmFilePrev").style.display = 'none';
    }

    // Show: input file
    if (document.querySelector("#"+WInputFile_name)) {
        document.querySelector("#"+WInputFile_name+"_obj_input").style.display = 'block';
    }
}
