<?php
/**
 * Register the service provider in composer.json
 *
 *  "post-package-update": [
 *      "@php artisan vendor:publish --tag crudcore --force"
 *  ]
 *
 */

 namespace angelrove\CrudCore;

use Illuminate\Support\ServiceProvider;

class CrudCoreServiceProvider extends ServiceProvider
{
    public function boot()
    {
        // $this->loadViewsFrom(__DIR__ . '/resources', 'datatables');
        // $this->registerCommands();

        // Publishes ---
        $listFiles = [
            __DIR__ . '/dashboard/assets/style.css' => CrudCore::assets_path('dashboard/style.css'),
            __DIR__ . '/DataTables/assets/lib.js' => CrudCore::assets_path('DataTables/lib.js'),

            __DIR__ . '/DbTables/assets/lib.js'    => CrudCore::assets_path('DbTables/lib.js'),
            __DIR__ . '/DbTables/assets/style.css' => CrudCore::assets_path('DbTables/style.css'),

            __DIR__ . '/FormInputs/assets/file.js' => CrudCore::assets_path('FormInputs/file.js'),
            __DIR__ . '/FormInputs/assets/autocomplete.js' => CrudCore::assets_path('FormInputs/autocomplete.js'),

            __DIR__ . '/dashboard/views/backpack'   => CrudCore::views_path(),
            __DIR__ . '/dashboard/views/components' => CrudCore::components_path(),
        ];

        $this->publishes($listFiles, 'crudcore');

        // $this->publishes([
        //     __DIR__ . '/config/config.php' => config_path(self::PACKAGE_NAME.'.php'),
        // ], 'config');
    }

    /**
     * Register commands.
     */
    protected function registerCommands()
    {
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
    }
}
