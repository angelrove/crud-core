<?php
use angelrove\CrudCore\Local;

Local::$t['delete'] = 'Eliminar';
Local::$t['new']    = 'Nuevo';
Local::$t['accept'] = 'Aceptar';
Local::$t['save']   = 'Guardar';
Local::$t['save_continue'] = 'Guardar y continuar';
Local::$t['save_and_new']  = 'Insertar otro &raquo;';
Local::$t['close']  = 'Cancelar';
Local::$t['view']   = 'Ver';
Local::$t['Search'] = 'Buscar';
Local::$t['close_session'] = 'Cerrar sesión';
Local::$t['WList_del_m'] = "Va a eliminar el registro. ¿Está seguro?";
Local::$t['GenQuery_error_obliga'] = 'campo obligatorio';
Local::$t['GenQuery_error_unique'] = 'está siendo utilizado';
Local::$t['Saved'] = '¡Guardado!';
Local::$t['Showing'] = 'Mostrando';
Local::$t['to'] = 'a';
Local::$t['of'] = 'de';
Local::$t['Username'] = 'Usuario';
Local::$t['Password'] = 'Contraseña';
