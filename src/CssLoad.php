<?php
/**
 * @author José A. Romero Vegas <jangel.romero@gmail.com>
 * 2011
 *
 */

namespace angelrove\CrudCore;

class CssLoad
{
    use CssJsLoader\LoaderTrait;

    //---------------------------------------------------------------------
    public static function get(): void
    {
        self::$called_get = true;

        echo '<!-- Imported with angelrove\CssLoad -->' . PHP_EOL .
             self::importFiles();
    }
    //---------------------------------------------------------------------
    private static function importFiles(): string
    {
        $strImports = '';

        foreach (self::$list_files_http as $file) {
            $strImports .= '<link type="text/css" href="' . $file . '" rel="stylesheet">' . PHP_EOL;
        }

        foreach (self::$list_files as $file) {
            $strImports .= '<link type="text/css" href="' . asset($file) . '" rel="stylesheet">' . PHP_EOL;
        }

        return $strImports;
    }
    //---------------------------------------------------------------------
}
