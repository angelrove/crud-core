<?php
/**
 * @author José A. Romero Vegas <jangel.romero@gmail.com>
 * 2011
 *
 */

namespace angelrove\CrudCore\CssJsLoader;

trait LoaderTrait
{
    // Status
    private static bool $called_get  = false;

    // Adds
    private static $list_files = [];
    private static $list_files_http = [];

    //---------------------------------------------------------------------
    public static function add(mixed $files): void
    {
        self::check_get_calls();

        if (!is_array($files)) {
            $files = [$files];
        }

        foreach ($files as $file) {
            // http files ---
            if (self::is_http($file)) {
                self::$list_files_http[$file] = $file;
            }
            // Local files ---
            else {
                self::$list_files[] = $file;
            }
        }
    }
    //---------------------------------------------------------------------
    // Private
    //---------------------------------------------------------------------
    private static function is_http(string $file): bool
    {
        return (strpos($file, '//') === false)? false : true;
    }
    //---------------------------------------------------------------------
    private static function check_get_calls(): void
    {
        if (self::$called_get == true) {
            throw new \Exception("JsLoad error: called 'add()' after 'get()'", 1);
        }
    }
    //---------------------------------------------------------------------
}
