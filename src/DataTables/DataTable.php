<?php
/**
 * @author José A. Romero Vegas <jangel.romero@gmail.com>
 *
 */

namespace angelrove\CrudCore\DataTables;
use angelrove\CrudCore\CrudUrl;
use angelrove\CssJsLoad\CssJsLoad;
use angelrove\CssJsLoad\JsLoad;

class DataTable
{
    private $id_control;
    private $id_component;
    private $columns;
    private $defaultOrder = ['index'=>0, 'type'=>"desc"];
    private $serverSide = false;
    private $stateSave  = false;

    private $rowOptions = array();
    private $renderOptions = 'false';
    private $bt_new = 'false';
    private $tableBordered = 'table-bordered';

    // Events
    private $action_search;
    private $event_new;
    private $event_update;

    //-------------------------------------------------------
    // PUBLIC
    //-------------------------------------------------------
    public function __construct($id_control, array $columns)
    {
        $this->id_control = $id_control;
        $this->columns    = $columns;

        // Conf ---
        $this->id_component = 'datatable_'.$this->id_control;

        // Events ---
        $this->laravelEvents();

        // css / js ---
        JsLoad::add([
            'https://cdn.datatables.net/v/bs4/dt-1.10.21/datatables.min.js',
            'https://cdn.datatables.net/v/bs4/dt-1.10.21/datatables.min.css'
        ]);
        JsLoad::add(__DIR__ . '/assets/lib.js');
    }
    //-------------------------------------------------------
    // Events
    //-------------------------------------------------------
    private function laravelEvents()
    {
        $this->event_cancel = CrudUrl::getRoute();
        $this->event_new    = CrudUrl::getRoute('create');
        $this->event_update = CrudUrl::getRoute('edit', '#id#');
        $this->event_delete = CrudUrl::getRoute('destroy', '#id#');

        $this->action_search = CrudUrl::getRoute();
        $this->url_ajax_data = CrudUrl::getRoute('json-list');
    }
    //-------------------------------------------------------
    // SETTERS
    //-------------------------------------------------------
    public function setDefaultOrder($index, $type = 'ASC')
    {
        $this->defaultOrder['index'] = $index;
        $this->defaultOrder['type']  = $type;
    }
    //-------------------------------------------------------
    public function renderOptions()
    {
        $this->renderOptions = 'true';
    }
    //-------------------------------------------------------
    public function setTableBordered($flag)
    {
        $this->tableBordered = ($flag === false)? '' : 'table-bordered';
    }
    //-------------------------------------------------------
    public function setServerSide($flag)
    {
        $this->serverSide = $flag;
    }
    //-------------------------------------------------------
    public function setStateSave($flag)
    {
        $this->stateSave = $flag;
    }
    //-------------------------------------------------------
    // Option buttons
    //-------------------------------------------------------
    public function rowOption($event, $title = '', $key = '')
    {
        $this->rowOptions[] = [
            'key'   => $key,
            'event' => $event,
            'title' => $title,
        ];
    }
    //-------------------------------------------------------
    public function showNew()
    {
        $this->bt_new = 'true';
    }
    //-------------------------------------------------------
    public function showUpdate()
    {
        $this->rowOption(
            $this->event_update,
            "<i class='la la-edit'></i> Edit",
            'op_update'
        );
    }
    //-------------------------------------------------------
    public function showDelete()
    {
        $this->rowOption(
            $this->event_delete,
            "<i class='la la-trash'></i> Del",
            'op_delete'
        );
    }
    //--------------------------------------------------------------
    // Searcher
    //--------------------------------------------------------------
    public function searcher()
    {
        return <<<EOD
        <form class="FormSearch form-inline well well-sm"
              role="search"
              name="search_form"
              method="get"
              action="$this->action_search">
        EOD;
    }
    //-------------------------------------------------------
    public function searcher_bt()
    {
        echo '<button type="submit" class="btn btn-primary btn-sm">Search</button>';
    }
    //-------------------------------------------------------
    public function searcher_END()
    {
        return '</form>';
    }
    //-------------------------------------------------------
    // HELPERS
    //-------------------------------------------------------
    public static function helper_parseQueryData($data)
    {
        if (!$data) {
            return '[]';
        }

        $ret = new \stdClass;

        // $ret->_page = 1;
        // $ret->_items = 1;
        // $ret->_totalItems = count($data);
        // $ret->_totalPages = 1;

        // $ret->_links = new stdClass;
        // $ret->_links->firstPage = 'xxxx';
        // $ret->_links->prevPage = 'xxxx';
        // $ret->_links->nextPage = 'xxxx';
        // $ret->_links->lastPage = 'xxxx';

        $ret->data = $data;

        return json_encode($ret);
    }
    //-------------------------------------------------------
    // PRIVATE
    //-------------------------------------------------------
    private function getOptionsBt()
    {
        $btOptions = array();

        foreach ($this->rowOptions as $data) {
            $btOptions[] = "<a href='$data[event]' class='btn btn-link btn-sm $data[key]'>$data[title]</a>";
        }

        $listButtons = '';
        if ($btOptions) {
            $listButtons = implode(' &nbsp; ', $btOptions);
        }

        return $listButtons;
    }
    //-------------------------------------------------------
    private function getJsColumns()
    {
        $columns = '';
        foreach ($this->columns as $column) {
            $orderable = ($column->orderable)? 'true':'false';
            $paramsType = ($column->type)? ",className:'".$column->type."'" : '';
            $columns .= " { orderable: $orderable, data: '".$column->name."' $paramsType },\n";
        }

        return $columns;
    }
    //-------------------------------------------------------
    // Get HTML
    //-------------------------------------------------------
    public function get()
    {
        // Js ------
        $dtColumns = $this->getJsColumns();
        $btOptions = $this->getOptionsBt();

        // Render types ---
        $colsRender_render   = array();
        $colsRender_datetime = array();
        $colsRender_bool     = array();
        $colsRender_image    = array();

        foreach ($this->columns as $key => $column) {
            if ($column->type == 'datetime') {
                $colsRender_datetime[] = $key;
            } elseif ($column->type == 'boolean') {
                $colsRender_bool[] = $key;
            } elseif ($column->type == 'image') {
                $colsRender_image[] = $key;
            } elseif ($column->type == 'render') {
                $colsRender_render[] = $key;
            }
        }

        $colsRender_render   = implode(',', $colsRender_render);
        $colsRender_datetime = implode(',', $colsRender_datetime);
        $colsRender_bool     = implode(',', $colsRender_bool);
        $colsRender_image    = implode(',', $colsRender_image);

        // Columns head ----
        $strColumnsHead = '';
        foreach ($this->columns as $column) {
            $strColumnsHead .= '<th>'.$column->title.'</th>';
        }

        // View ------------
        ob_start();
        require_once __DIR__ . '/views/datatable.view.php';
        return ob_get_clean();
    }
    //--------------------------------------------------------------
}
