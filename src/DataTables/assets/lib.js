/**
 * WDatatables with https://datatables.net
 * José Angel Romero <jangel.romero@gmail.com>
 *
 */

String.prototype.replaceAll = function(search, replacement) {
    var target = this;
    return target.replace(new RegExp(search, 'g'), replacement);
};

$(document).ready(function() {
    // console.log(href_ajax);
    // console.log(id_component);

    var dataTable = $('#'+id_component).DataTable( {
        "order": [[ defaultOrder_index, defaultOrder_type ]],
        "select": true,
        "processing": true,
        "serverSide": serverSide,
        "stateSave": stateSave,
        // "deferRender": true,

        "ajax": { dataSrc: 'data', url: href_ajax },

        // Option buttons ---
        // dom: 'Bfrtip',
        // buttons: [ 'print', 'csvHtml5', 'copyHtml5' ],

        // Columns ---
        aoColumns: dt_cols,

        // Render types ---
        columnDefs: [
            {
                // render columns
                render: function ( data, type, row ) {
                   return dt_render(data, type, row);
                },
                targets: colsRender_render
            },
            {
                // render options column
                render: function ( data, type, row ) {
                   var value = btOptions.replaceAll("#id#", row.id);
                   if (renderOptions) {
                       value = dt_render_options(row, value);
                   }

                   return value;
                },
                targets: [-1]
            },
            {
                // type: datetime
                render: function ( data, type, row ) {
                   if(!data) return '';
                   if(type === "sort" || type === "type") return data;

                   // return moment(data*1000).format("YYYY/MM/DD HH:mm");
                   // var d = new Date(data*1000);
                   var d = new Date(data);
                   return d.toLocaleString(false,
                            { formatMatcher:"best fit",
                              year:"numeric", month:"2-digit", day:"2-digit", hour:"2-digit", minute:"2-digit" }
                          );
                },
                targets: colsRender_datetime
            },
            {
                // type: bool
                render: function ( data, type, row ) {
                    if(data == true) {
                       return '<i class="glyphicon glyphicon-check"></i>';
                    } else {
                       return '';
                    }
                },
                targets: colsRender_bool
            },
            {
                // type: image
                render: function ( data, type, row ) {
                    if(data) {
                       return '<img src="'+data+'">';
                    } else {
                       return '';
                    }
                },
                targets: colsRender_image
            }
        ]

    });

/*
    dataTable.on( 'draw', function () {
        // Load row state ---
        selectPersistedRows(dataTable);
    } );
*/
    // Option: New --------------
    if (bt_new) {
       $("th.options").append('<a href="'+event_new+'" class="btn btn-sm btn-success"><i class="fa fa-plus"></i> New</a>');
    }

    // Option: Del --------------
    dataTable.on( 'click', 'a.op_delete', function (e) {
       e.preventDefault();
       if (confirm('Are you sure?')) {
          persistSelection('');
          location.href = $(this).attr('href');
       }
    });

    // Options buttons (siempre seleccionar la row) ---
    dataTable.on( 'click', '.options a', function (e) {
       // truco para que siempre entre en la linea 116
       dataTable.$('tr.selected').removeClass('selected');
    });

    // Row event handler --------
    dataTable.on('click', 'tbody tr', function(event) {
       if ( $(this).hasClass('selected') ) {
           persistSelection('');
       } else {
           // dataTable.$('tr.selected').removeClass('selected'); // single selection
           persistSelection( dataTable.row(this).index());
       }
    } );

});

//----------------------------------------------------------
// Save row state
//----------------------------------------------------------
function selectPersistedRows(table)
{
   // sessionStorage.clear();
   if (!(sessionStorage[id_component])) {
      return;
   }

   // console.log("index: "+sessionStorage[id_component]);
   var rowKey = sessionStorage[id_component];
   table.rows( rowKey ).nodes().to$().addClass("selected");
}
//----------------------------------------------------------
function persistSelection(index)
{
   if (!(sessionStorage[id_component])) {
       sessionStorage[id_component] = "";
   }
   sessionStorage[id_component] = index;
   // console.log(sessionStorage[id_component]);
}
//----------------------------------------------------------
