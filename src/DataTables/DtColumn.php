<?php
/**
 * @author José A. Romero Vegas <jangel.romero@gmail.com>
 *
 */
namespace angelrove\CrudCore\DataTables;

class DtColumn
{
    public $name;
    public $title;
    public $type;
    public $orderable;

    public function __construct($name, $title, $type = '')
    {
        $this->name  = $name;
        $this->title = $title;
        $this->type = $type;
        return $this;
    }

    public function sortable(bool $sortable = true)
    {
        $this->orderable = $sortable;
        return $this;
    }
}
