<style>
.datatable { border: 1px solid #ddd; }
.datatable td.datetime  { width:90px; white-space: nowrap; }
.datatable td.boolean   { width: 70px; text-align:center; }
.datatable td.image     { width: 100px; }
.datatable td.image img { width: 100%; border: 1px solid #ddd; border-radius: 7px; }
.datatable td.price     { width: 66px; }

.datatable .options { width:90px; white-space: nowrap; text-align: center; }
.datatable tr.selected td.options { background: white; }
.datatable tr.selected td.options a { color: initial !important; }
</style>

<script>
var id_component = '<?=$this->id_component?>';
var dt_cols = [
  <?=$dtColumns?>
  {
    data: null, targets: -1, className:'options', orderable:false, searchable:false, width:108,
    defaultContent: btOptions
  }
];

var colsRender_render   = [<?=$colsRender_render?>];
var colsRender_datetime = [<?=$colsRender_datetime?>];
var colsRender_bool     = [<?=$colsRender_bool?>];
var colsRender_image    = [<?=$colsRender_image?>];
var renderOptions = <?=$this->renderOptions?>;
var serverSide    = <?=($this->serverSide)? 'true' : 'false'?>;
var stateSave     = <?=($this->stateSave)? 'true' : 'false'?>;

var defaultOrder_index = <?=$this->defaultOrder['index']?>;
var defaultOrder_type  = "<?=$this->defaultOrder['type']?>";

// Events ---
var btOptions = "<?=$btOptions?>";
var href_ajax = '<?=$this->url_ajax_data?>';
var event_new = '<?=$this->event_new?>';
var bt_new = <?=$this->bt_new?>;
</script>

<table id="<?=$this->id_component?>"
       class="datatable table table-hover
              table-striped
              <?=$this->tableBordered?>"
       data-page-length="25"
       style="width:100%">
    <thead><tr>
      <?=$strColumnsHead?>
      <th></th>
    </tr></thead>
</table>
