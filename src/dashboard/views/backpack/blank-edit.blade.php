@extends('CrudCore::blank-secc')

@section('content')
<div class="row">
    <div class="col-md bold-labels">
        <div class="card">
            <div class="card-body row">
                @yield('form')

                <div class="text-center form-group col-sm-12">
                    {!! Form::submit('Accept', ['class' => 'btn btn-primary']) !!} &nbsp;
                    <a class="btn btn-default" href="{!! \angelrove\CrudCore\CrudUrl::getRoute() !!}">Cancel</a>
                </div>
                {!! Form::close() !!}
            </div>
        </div>

        <div>
            @yield('other_content')
        </div>
    </div>
</div>
@stop
