
<!-- Messages.blade -->
<div id="admin-messages">
    @if ($errors->any())
    <div class="callout callout-warning">
       <p><b>Errors!</b></p>
       @foreach ($errors->all() as $error)
       <p>- {{ $error }}</p>
       @endforeach
    </div>
    @endif

    @if (isset($message))
    <div class="callout callout-notify">
       <p>{{ $message }}</p>
    </div>
    @endif
</div>

@push('after_scripts')
  <script>
  $(document).ready(function() {
     $("#admin-messages").delay(9000).fadeOut();
  });
  </script>
@endpush
<!-- End Messages.blade -->