<?php
use angelrove\CrudCore\CrudCore;
use angelrove\CrudCore\JsLoad;
use angelrove\CrudCore\CssLoad;
?>

@extends(backpack_view('blank'))

@push('css')
  <?php CssLoad::add(CrudCore::VENDOR_DIR.'/dashboard/style.css'); ?>
@endpush

@push('after_scripts')
  <?php JsLoad::get(); ?>
@endpush
@push('after_styles')
  <?php CssLoad::get(); ?>
@endpush

@section('header')
  @component('CrudCore::messages', ['message' => @$message, 'errors' => $errors])
  @endcomponent

  <div class="container-fluid">
    <h2>
      <span class="text-capitalize">
          {!! $title !!}
      </span>
    </h2>
  </div>
@stop
