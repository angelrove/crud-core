
@props([
    'type' => 'text',
    'label' => $name,
    'name' => null,
    'value' => null,
    'required' => null,
    'disabled' => null,
])

<?php
use angelrove\CrudCore\FormInputs\FormInputs;
$flagRequired = ($required) ? ' *':'';
?>

<div class="form-group col-sm-12">
    {!! Form::label($label.$flagRequired, null, ['class' => '']) !!}
    {!! FormInputs::getInput($type, $name, $value, $required, $disabled) !!}
</div>
