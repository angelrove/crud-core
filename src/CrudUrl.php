<?php

/**
 * @author Jos� A. Romero Vegas <jangel.romero@gmail.com>
 *
 */

namespace angelrove\CrudCore;

class CrudUrl
{
    //-------------------------------------------------------------
    public static function getRoute(string $event = '', bool $haveId = false): string
    {
        if (!$event) {
            return self::getSeccRoute();
        }

        $secc = self::getSecc();

        $routeStr = $secc . '.' . $event;

        $route = '';
        if (\Route::has($routeStr)) {
            $route = ($haveId) ? route($routeStr, '#id#') : route($routeStr);
        }

        // dump($route);
        return $route;
    }
    //-------------------------------------------------------------
    public static function getRouteParams($event = '', $objectId = '', $id = '', array $params = []): string
    {
        $seccRoute = self::getSeccRoute();
        $urlParams = self::getParams('GET', $event, $objectId, $id, $params);
        return $seccRoute . $urlParams;
    }
    //-------------------------------------------------------------
    public static function getParams($method='GET', $event = '', $objectId = '', $id = '', array $params = []): string
    {
        // paramsList ---
        $listParams = [
            'event' => $event,
            'objectId' => $objectId,
        ];
        if ($id) {
            $listParams[] = 'id=' . $id;
        }
        $listParams += $params;

        //--------
        $htmlParams = '';
        if ($method == 'GET') {
            foreach($listParams as $name => $value) {
                $htmlParams .= "&$name=$value";
            }
            $htmlParams = '?' . ltrim($htmlParams, '&');
        } else {
            foreach($listParams as $name => $value) {
                $htmlParams .= "<input type='hidden' name='$name' value='$value'>\n";
            }
        }

        return $htmlParams;
    }
    //-------------------------------------------------------------
    public static function getSeccRoute(): string
    {
        $routePrefix = self::getPrefix();
        $secc = self::getSecc();

        return url($routePrefix . '/' . $secc);
    }
    //-------------------------------------------------------------
    public static function getSecc(): string
    {
        $segment = (self::getPrefix()) ? 2 : 1;
        $secc = \Request::segment($segment);
        // $secc = \Request::getpathInfo();

        return $secc;
    }
    //-------------------------------------------------------------
    private static function getPrefix(): string
    {
        return config('backpack.base.route_prefix');
    }
    //-------------------------------------------------------------
}
