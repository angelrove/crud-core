# CrudCore

Basic CRUD components and utils for create Admin panels in Laravel.
(Use Laravel Collective)[laravelcollective.com]

## Install

    composer require angelrove/crud-core
    php artisan vendor:publish --tag crudcore

Register the service provider in 'composer.json':

    "post-package-update": [
        "@php artisan vendor:publish --tag crudcore --force"
    ]

## Services

- EventStatus: for Session control
- JsLoad

## Components

- DbTables
- FormInputs
- Backpack: views and assets

## Usage

    ```php
    class FundingsController extends Controller
    {
        public function __construct()
        {
            CrudCore::init();
            ...
        }
        ...
    }
    ```

## EventStatus

    ```php
    $componentId = 'fundings';

    // Filters
    $f_country = EventStatus::getData($componentId, 'f_location');
    $f_search  = EventStatus::getData($componentId, 'search');
    ```

## DbTables

Create customised, fully editable tables easily.

    ```php
    $columns = [
        (new DtColumn('id', 'Id'))->sortable(),
        (new DtColumn('created_at', 'created at'))->type('datetime'),
        (new DtColumn('name', 'Organization'))->sortable(),
        (new DtColumn('announced_date', 'Announced date'))->type('date'),
        (new DtColumn('money_raised', 'Money raised $'))->align('right')->type('money_short'),
        (new DtColumn('total_amount', 'Total $'))->align('right')->type('money'),
    ];

    $dbTable = new DbTables($componentId, Funding::select(), $columns);
    $dbTable->showNew();
    $dbTable->showUpdate();

    {!! $dataTable->get() !!}
    ```

## DataTables

Create DataTable component from PHP (https://datatables.net/)

## FormInputs

The Form component is a tool to help you solve the problem of allowing end-users to interact with the data and modify the data in your application.

    FormInputs::password();
    FormInputs::text();
    FormInputs::textarea();
    FormInputs::hidden();
    FormInputs::select();
    FormInputs::radios();
    FormInputs::check();
    FormInputs::file();
    FormInputs::email();
    FormInputs::number();
    FormInputs::price();
    FormInputs::percent();
    FormInputs::datetime();
    FormInputs::date();
    FormInputs::month();
    FormInputs::week();
    FormInputs::time();
    FormInputs::url();
    FormInputs::tel();
    FormInputs::autocomplete();

## Backpack: views and assets

Two views are provided:

- blank-secc

    ```php
    @extends('CrudCore::blank-secc')

    @section('content')
        {!! $tableUsers->get() !!}
    @stop
    ```

- blank-edit

View: https://laravelcollective.com/docs/6.x/html#form-model-binding

    ```php
    @extends('CrudCore::blank-edit')

    @section('form')
    {!! Form::model($data, [
        'route' => [$route, $data->id],
        'method' => $method,
        'files'  => $files,
    ]) !!}

        <x-crudcore.input name="name"  />
        <x-crudcore.input name="email" type="email" required />
        <?=FormInputs::price('money_raised', $data->money_raised)->title('Money raised')->required()->container()->get() ?>
    @stop
    ```
